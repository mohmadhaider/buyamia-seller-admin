// -- React and related libs
import React from "react";
import { Switch, Route, Redirect } from "react-router";
import { HashRouter } from "react-router-dom";

// -- Redux
import { connect } from "react-redux";

// -- Custom Components
import LayoutComponent from "./components/Layout/Layout";
import ErrorPage from "./pages/error/ErrorPage";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";

// -- Redux Actions
import { logoutUser } from "./actions/auth";

// -- Third Party Libs
import { ToastContainer } from "react-toastify";

// -- Services
import isAuthenticated from "./services/authService";

// -- Component Styles
import "./styles/app.scss";
import Profile from "./pages/profile/Profile";
import Product from "./pages/product/Product";
import EmailVerification from "./pages/authtoken/EmailVerification";
import CompanyDetails from "./pages/register/CompanyDetails";


const PrivateRoute = ({ dispatch, component, ...rest }) => {
  if (!isAuthenticated(JSON.parse(localStorage.getItem("authenticated")))) {
    dispatch(logoutUser());
    return <Redirect to="/login" />;
  } else {
    return <Route {...rest} render={() => React.createElement(component)} />;
  }
};

const App = (props) => {
  return (
    <div>
      <ToastContainer />
      <HashRouter>
        {/* <Header /> */}
        <Switch>
          <Route
            path="/"
            exact
            render={() => <Redirect to="/template/home" />}
          />
          <Route
            path="/"
            exact
            render={() => <Redirect to="/template/home" />}
          />
          <PrivateRoute
            path="/template"
            dispatch={props.dispatch}
            component={LayoutComponent}
          />
          <Route path="/login" exact component={Login} />
          <PrivateRoute
            path="/sellerapproval"
            dispatch={props.dispatch}
            component={LayoutComponent}
          />
          <PrivateRoute
            path="/sellerapproval/:id"
            dispatch={props.dispatch}
            component={LayoutComponent}
          />
          <PrivateRoute
            path="/profile"
            dispatch={props.dispatch}
            component={LayoutComponent}
          />
          <Route path="/error" exact component={ErrorPage} />
          <Route path="/register" exact component={Register} />
          <Route path="/companydetails" exact component={CompanyDetails} />
          <Route path="/emailverification/:token" exact component={EmailVerification} />
          <Route path="/product" exact component={Product} />

          <Route component={ErrorPage} />
          <Route
            path="*"
            exact={true}
            render={() => <Redirect to="/error" />}
          />
        </Switch>
      </HashRouter>
    </div>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});


export default connect(mapStateToProps)(App);
