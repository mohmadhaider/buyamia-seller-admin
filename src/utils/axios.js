import axios from "axios";
import ReactDOM from "react-dom";
// import {  message, Spin } from "antd";
import { CircularProgress, Snackbar } from "@material-ui/core";
import {Alert} from 'reactstrap'
import { createBrowserHistory  } from "history";
// import i18n from "./i18n";

const Axios = axios.create({
  baseURL: process.env.REACT_APP_SERVICE_URL,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json, text/plain, !/!",
    // "Access-Control-Expose-Headers": "*",
  },
  timeout: 60000,
  // withCredentials: true,
});

let requestCount = 0;

function showLoading( customMessage ) {
  if (requestCount === 0) {
    const dom = document.createElement("div");
    dom.setAttribute("id", "loading");
    document.body.appendChild(dom);
    ReactDOM.render(
      <Snackbar
        color="dark"
        open={true}
        anchorOrigin={{ horizontal: "center", vertical: "top" }}
        keyboard={false}
        style={{ height: "100%", width: "100%" }}
      >
        <Alert
          className="text-center"
          color="dark"
          style={{ color: "rgb(255, 230, 0)" }}
        >
          <CircularProgress
            style={{ color: "rgb(255, 230, 0)" }}
          ></CircularProgress>
          <br />
          {customMessage ? customMessage : "Loading...!!"}
        </Alert>
      </Snackbar>,
      dom
    );
  }
  requestCount++;
}

function hideLoading() {
  requestCount--;
  if (requestCount === 0) {
    document.body.removeChild(document.getElementById("loading"));
  }
}

Axios.interceptors.request.use(
  (config) => {
    if (!config.hideLoading) {
      showLoading();
    }
    return config;
  },
  (err) => {
    if (!err.config.hideLoading) {
      hideLoading();
    }
    return Promise.reject(err);
  }
);

Axios.interceptors.response.use(
  (res) => {
    if (!res.config.hideLoading) {
      hideLoading();
    }
    return res;
  },
  (err) => {
    if (err.message === "Network Error") {
      showLoading("Network Error");
      setTimeout(function(){
        hideLoading();
      },6000);
      // message.error();
      // err.showed = true;
    }
    if (err.code === "ECONNABORTED") {
      showLoading("Network TimeOut");
      setTimeout(function () {
        hideLoading();
      }, 6000);
      // message.error(i18n.t("msg.timeoutError"));
      // err.showed = true;
    }
    if (err?.response?.status === 401) {
      createBrowserHistory().replace("/Home");
      window.location.reload();
      showLoading("Authentication Error");
      setTimeout(function () {
        hideLoading();
      }, 6000);
      // message.error(i18n.t("msg.authFailedError"));
      // err.showed = true;
    }

    if (!err.config.hideLoading) {
      hideLoading();
    }
    return Promise.reject(err);
  }
);

export default Axios;