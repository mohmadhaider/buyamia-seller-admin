import React, { useState } from 'react'
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import CloseIcon from '@material-ui/icons/Close';

import {
    Card,
    CardBody,
    CardTitle,
} from "reactstrap";
import OrderQuantities from '../OrderQuantities';

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
        width: 500,
    },
}))(MuiDialogContent);
const MediaModal = ({ show, setShowModal, updateData }) => {
    const [tempState, setTempState] = useState(updateData);

    let saveData = () => {
        if (updateData) {
            // let tempArray = JSON.parse(localStorage.getItem("OrderQuantities"));
            // console.log("id ==>",tempArray[updateData])
            // tempArray[updateData.id] = [tempState];
            // console.log("tempArray",tempArray);
            // setOrderQuantities(tempArray);
            // localStorage.setItem("OrderQuantities",JSON.stringify(orderQuantities));
            // setShowModal(false); 
        }
        else {
            // let orderArr = orderQuantities;
            // orderArr.push(tempState);
            // setOrderQuantities(orderArr);
            // localStorage.setItem("OrderQuantities", JSON.stringify(orderArr));
            // setShowModal(false);
        }
    }

    let changeEvent = (e) => {
        setTempState({ ...tempState, [e.target.name]: e.target.value })
    }

    const handleClose = () => {
        setShowModal(false);
    };


    return (
        <Dialog
            onClose={handleClose}
            aria-labelledby="customized-dialog-title"
            open={true}
        >
            <DialogContent onClose={handleClose}>
                <Card className="main-card mb-3">
                    <CardBody>
                        <button className=' btn-dark float-right' onClick={handleClose}><CloseIcon /></button>
                        <CardTitle className="mt-5 h3 font-weight-bolder">
                            Media
                        </CardTitle>
                        <form onSubmit={saveData} className='mt-2'>
                            <div className="row m-3" >
                                <select class="form-control" name="variantType" value={tempState?.variantType} onChange={(e) => { changeEvent(e) }}>
                                    <option selected>Select Variant</option>
                                    <option>Color</option>
                                </select>
                            </div>
                            <div className="row m-3" >
                                <select class="form-control" name="variantOptions" value={tempState?.variantOptions} onChange={(e) => { changeEvent(e) }}>
                                    <option selected>Select Options</option>
                                    <option>Red</option>
                                    <option>Blue</option>
                                    <option>Green</option>
                                </select>
                            </div>
                            <div className="row m-3" >
                                <label for="exampleFormControlFile1">Upload Variant Image</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" />
                            </div>
                            <div className="row m-2">
                                <button type="submit" className="btn btnPrimary justify-content-center">Save</button>
                            </div>
                        </form>
                    </CardBody>
                </Card>
            </DialogContent>
        </Dialog>
    )
}

export default MediaModal
