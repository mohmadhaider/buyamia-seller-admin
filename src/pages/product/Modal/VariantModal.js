import React, { useState } from 'react'
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import CloseIcon from '@material-ui/icons/Close';

import {
    Card,
    CardBody,
    CardTitle,
} from "reactstrap";
import OrderQuantities from '../OrderQuantities';

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
        width: 500,
    },
}))(MuiDialogContent);

const VariantModal = ({ show, setShowModal, updateData }) => {
    const [tempState, setTempState] = useState(updateData);
    const [variant, setVariant] = useState([])

    let saveData = () => {
        if (updateData) {
            // let tempArray = JSON.parse(localStorage.getItem("OrderQuantities"));
            // console.log("id ==>", tempArray[updateData])
            // tempArray[updateData.id] = [tempState];
            // console.log("tempArray", tempArray);
            // setOrderQuantities(tempArray);
            // localStorage.setItem("OrderQuantities", JSON.stringify(orderQuantities));
            // setShowModal(false); 
        }
        else {
            let variantArr = variant;
            variantArr.push(tempState);
            setVariant(variantArr);
            localStorage.setItem("Variant", JSON.stringify(variantArr));
            setShowModal(false);
        }
    }

    let changeEvent = (e) => {
        setTempState({ ...tempState, [e.target.name]: e.target.value })
    }

    const handleClose = () => {
        setShowModal(false);
    };


    return (
        <Dialog
            onClose={handleClose}
            aria-labelledby="customized-dialog-title"
            open={true}
        >
            <DialogContent onClose={handleClose}>
                <Card className="main-card mb-3">
                    <CardBody>
                        <button className=' btn-dark float-right' onClick={handleClose}><CloseIcon /></button>
                        <CardTitle className="mt-5 h3 font-weight-bolder">
                            Variant
                        </CardTitle>
                        <form onSubmit={saveData} className='mt-2'>
                            <div className="row m-2" >
                                <input type="text" className="form-control" name='variantname' value={tempState?.variant} onChange={(e) => { changeEvent(e) }} placeholder="Variant Name" />
                            </div>
                            <div className="row m-2" >
                                <input type="text" className="form-control" name='options' value={tempState?.options} onChange={(e) => { changeEvent(e) }} placeholder="Options ( Comma Separated e.g. Red, Blue, Black)" />
                            </div>
                            <div className="row m-2">
                                <button type="submit" className="btn btnPrimary justify-content-center">Save</button>
                            </div>
                        </form>
                    </CardBody>
                </Card>
            </DialogContent>
        </Dialog>
    )
}

export default VariantModal
