import React, { useState } from 'react'

import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import CloseIcon from '@material-ui/icons/Close';

import {
    Card,
    CardBody,
    CardTitle,
} from "reactstrap";
import OrderQuantities from '../OrderQuantities';

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
        width: 500,
    },
}))(MuiDialogContent);

const OrderQuantitiesModal = ({ show, setShowModal, updateData }) => {

    const [tempState, setTempState] = useState(updateData);
    const [orderQuantities, setOrderQuantities] = useState(localStorage.getItem("OrderQuantities") ? JSON.parse(localStorage.getItem("OrderQuantities")) : []);
    let saveData = () => {
        if(updateData)
        {
            let tempArray = JSON.parse(localStorage.getItem("OrderQuantities"));
            console.log("id ==>",tempArray[updateData])
            tempArray[updateData.id] = [tempState];
            console.log("tempArray",tempArray);
            setOrderQuantities(tempArray);
            localStorage.setItem("OrderQuantities",JSON.stringify(orderQuantities));
            setShowModal(false); 
        }
        else
        {
            let orderArr = orderQuantities;
            orderArr.push(tempState);
            setOrderQuantities(orderArr);
            localStorage.setItem("OrderQuantities", JSON.stringify(orderArr));
            setShowModal(false);
        }
    }

    let changeEvent = (e) => {
        setTempState({ ...tempState, [e.target.name]: e.target.value })
    }

    const handleClose = () => {
        setShowModal(false);
    };


    return (
        <Dialog
            onClose={handleClose}
            aria-labelledby="customized-dialog-title"
            open={true}
        >
            <DialogContent onClose={handleClose}>
                <Card className="main-card mb-3">
                    <CardBody>
                        <button className=' btn-dark float-right' onClick={handleClose}><CloseIcon /></button>
                        <CardTitle className="mt-5 h3 font-weight-bolder">
                            Order Quantities
                        </CardTitle>
                        <form onSubmit={saveData} className='mt-2'>
                            <div className="row m-2" >
                                <input type="text" className="form-control" name='minQty' value={tempState?.minQty} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Min Qty" />
                            </div>
                            <div className="row m-2" >
                                <input type="text" className="form-control" name='maxQty' value={tempState?.maxQty} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Max Qty" />
                            </div>
                            <div className="row m-2" >
                                <input type="text" className="form-control" name='price' value={tempState?.price} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Price Per Piece" />
                            </div>
                            <div className="row m-2" >
                                <input type="text" className="form-control" name='fromLeadtime' value={tempState?.fromLeadtime} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Estimate From Leadtime (In Days)" />
                            </div>
                            <div className="row m-2" >
                                <input type="text" className="form-control" name='toLeadtime' value={tempState?.toLeadtime} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Estimate From Leadtime (In Days)" />
                            </div>
                            <div className="row m-2">
                                <button type="submit" className="btn btnPrimary justify-content-center">Save</button>
                            </div>
                        </form>
                    </CardBody>
                </Card>
            </DialogContent>
        </Dialog>
    )
}

export default OrderQuantitiesModal
