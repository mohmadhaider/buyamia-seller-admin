import React,{useState} from 'react'
import ReactQuill from 'react-quill'

const RtfEditor = ({updateParentState}) => {
    const [richtext, setrichText] = useState('');

    const onChangeText = (val) => {
        setrichText(val);
        updateParentState(val);
    };

    return (
        <>
            <ReactQuill value={richtext}
                  onChange={(text)=>onChangeText(text)} />
        </>
    )
}

export default RtfEditor
