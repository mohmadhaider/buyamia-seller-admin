import React, { useState } from 'react';
import { styled } from '@mui/material/styles';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';


const IOSSwitch = styled((props) => (
    <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,
        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(16px)',
            color: '#fff',
            '& + .MuiSwitch-track': {
                backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
                opacity: 1,
                border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
                opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: '#33cf4d',
            border: '6px solid #fff',
        },
        '&.Mui-disabled .MuiSwitch-thumb': {
            color:
                theme.palette.mode === 'light'
                    ? theme.palette.grey[100]
                    : theme.palette.grey[600],
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
    },
    '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 22,
        height: 22,
    },
    '& .MuiSwitch-track': {
        borderRadius: 26 / 2,
        backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
    },
}));
const ProductInfoForm = (props) => {

    const [product, setProduct] = useState({});
    const [perishable, setPerishable] = useState(false);
    const [liquid, setLiquid] = useState(false);

    let changeEvent = (e) => {
        setProduct({ ...product, [e.target.name]: e.target.value })
    }
    let handleChange = (e, key) => {
        if (key === "perishable") {
            setPerishable(e.target.checked)
        }
        if (key === "liquid") {
            setLiquid(e.target.checked)
        }
        setProduct({ ...product, [key]: e.target.checked })
    }

    let saveData = () => {
        localStorage.setItem("ProductDetails", JSON.stringify(product));
        props.func();
    }

    return (
        <>
            <div className='container'>
                <form onSubmit={saveData}>
                    <p className='justify-content-center h3 px-4 mt-5' style={{ "color": "green" }}>Basic Details</p>
                    <div className="row m-2" >
                        <div className="col">
                            <input type="text" className="form-control" name='ProductName' value={product.ProductName} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Product Name (Title)" />
                        </div>
                    </div>
                    <div className="row m-2" >
                        <div className="col">
                            <textarea className="form-control" name='ProductDesc' rows="3" value={product.ProductDesc} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Product Description" />
                        </div>

                    </div>
                    <div className="row m-2">
                        <div className="col">
                            <input type="text" className="form-control" name='skuNo' value={product.skuNo} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your SKU No/Product No" />
                        </div>
                        <div className="col">
                            <input type="number" className="form-control" name='MOQ' value={product.MOQ} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your MOQ" />
                        </div>

                    </div>
                    <div className="row m-2">

                        <div className="col">
                            <div className="input-group">
                                <select className="custom-select" name="category" value={product.category} onChange={(e) => { changeEvent(e) }} id="category">
                                    <option selected>Category</option>
                                    <option value="1">Home Living</option>
                                    <option value="2">Art</option>
                                    <option value="3">Kitchen</option>
                                </select>
                            </div>
                        </div>
                        <div className="col">
                            <div className="input-group">
                                <select className="custom-select" name="subCategory" value={product.subCategory} onChange={(e) => { changeEvent(e) }} id="category">
                                    <option selected>Sub Category</option>
                                    <option value="1">Kitechen</option>
                                    <option value="2">Painting</option>
                                    <option value="3">Statues</option>
                                    <option value="4">Sculpture</option>
                                    <option value="5">Decoration</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="row m-2">
                        <div className="col">
                            <input type="text" className="form-control" name='rate' value={product.rate} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Rate" />
                        </div>
                        <div className="col">
                            <input type="text" className="form-control" name='msrp' value={product.msrp} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your MSRP" />
                        </div>

                    </div>
                    <div className="row m-2">
                        <div className="col">
                            <input type="text" className="form-control" name='Brand' value={product.Brand} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Brand Name" />
                        </div>
                        <div className="col">
                            <input type="text" className="form-control" name='modelNumber' value={product.modelNumber} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Model Number" />
                        </div>

                    </div>
                    <div className="row m-2">
                        <div className="col">
                            <input type="number" className="form-control" name='sampleRate' value={product.sampleRate} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Sample Rate" />
                        </div>
                        <div className="col">
                            <input type="number" className="form-control" name='Sampleqty' value={product.Sampleqty} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Sample Quantity" />
                        </div>

                    </div>
                    <div className="row m-2">
                        <div className="col-6">
                            <input type="text" className="form-control" name='hsCode' value={product.hsCode} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your HS Code" />
                        </div>
                    </div>
                    <div className="row m-2">
                        <div className="col-6">
                            <div className='row'>
                                <div className="col-4">
                                    <FormControlLabel name="perishable" value={perishable}
                                        checked={perishable}
                                        onChange={(e) => { handleChange(e, "perishable") }}
                                        control={<IOSSwitch sx={{ m: 1 }} />}
                                        label="Perishable"
                                    />
                                </div>
                                {
                                    perishable && (
                                        <div className="col-8">
                                            <input type="number" className="form-control" name='expiresIn' value={product.expiresIn} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Expires in Days" />
                                        </div>
                                    )

                                }
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="col">
                                <FormControlLabel name="liquid" value={liquid}
                                    checked={liquid}
                                    onChange={(e) => { handleChange(e, "liquid") }}
                                    control={<IOSSwitch sx={{ m: 1 }} />}
                                    label="Liquid"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row m-2">
                        <div className="col">
                            <button type="submit" className="btn btnPrimary justify-content-center">Save</button>
                        </div>
                    </div>
                </form>
            </div >
        </>
    )
}

export default ProductInfoForm
