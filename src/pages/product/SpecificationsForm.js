import React, { useState, useEffect } from 'react'
import Editor from './Editor'
import RtfEditor from './RtfEditor';
import Axios from "axios";

const SpecificationsForm = () => {

    const [product, setProduct] = useState({})
    const [country, setCountry] = useState([]);
    const [selectCountry, setSelectCountry] = useState('')
    let changeEvent = (e) => { 
        setProduct({...product,[e.target.name]:e.target.value})
    }
    let saveData = () => { }

    const onChangeCountry = (e) => {
        setSelectCountry(e.target.value);
      };

      const updateParentState = (value) => {
          setProduct({
            ...product,
            otherspecification: value
          });
      };

    useEffect(() => {
        Axios.get("https://countriesnow.space/api/v0.1/countries/capital").then(
          (data) => {
            setCountry(data.data.data);
          }
        );
      }, []);
    return (
        <>
            <div className='container'>
                <form onSubmit={saveData}>
                    <p className='justify-content-center h3 px-4 mt-5' style={{ "color": "green" }}>Specification</p>
                    <div className="row m-2" >
                        <div className="col">
                            <label>Product Features</label>
                            <input type="text" className="form-control" name='features' value={product.features} onChange={(e) => { changeEvent(e) }} placeholder="Product Features Comma Seprated (e.g. Adjutable,Reversible)" />
                        </div>
                        <div className="col">
                            <label>Product Application</label>
                            <input type="text" className="form-control" name='application' value={product.application} onChange={(e) => { changeEvent(e) }} placeholder="Product Application Comma Seprated (e.g. Cooking, Home)" />
                        </div>
                    </div>
                    <div className="row m-2">
                        <div className="col">
                            <label>Buyer Type</label>
                            <input type="text" className="form-control" name='buyertype' value={product.buyertype} onChange={(e) => { changeEvent(e) }} placeholder="Enter Buyer Type" />
                        </div>
                        <div className="col">
                            <label>Ocasion</label>
                            <input type="text" className="form-control" name='occasion' value={product.occasion} onChange={(e) => { changeEvent(e) }} placeholder="Enter Occasion (e.g. Winter)" />
                        </div>

                    </div>
                    <div className="row m-2">
                        <div className="col-6">
                            <label className="h6">Country of Origin</label>
                            <select
                                class="form-control"
                                name="selectCountry"
                                value={selectCountry}
                                onChange={onChangeCountry}
                            >
                                <option value="0" selected>
                                    Select Country
                                </option>
                                {country?.map((tempCountry) => (
                                    <option value={tempCountry.name}>{tempCountry.name}</option>
                                ))}
                            </select>
                        </div>
                        <div className="col-6">
                            <label>Shipping Details</label>

                            <input type="text" className="form-control" name='shipping' value={product.shipping} onChange={(e) => { changeEvent(e) }} placeholder="Enter shipping Details" />
                        </div>
                    </div>
                    <div className="row m-2">

                        <div className="col">
                            <label>Operating Instruction</label>
                            <textarea className="form-control" name='operatinginstruction' rows="2" value={product.operatinginstruction} onChange={(e) => { changeEvent(e) }} placeholder="Enter Operting Instruction" />
                        </div>
                    </div>
                    <div className="row m-2">

                        <div className="col">
                            <label>Handling Instruction</label>
                            <textarea className="form-control" name='handlinginstruction' rows="2" value={product.handlinginstruction} onChange={(e) => { changeEvent(e) }} placeholder="Enter Handling Instruction" />
                        </div>
                    </div>
                    <div className="row m-2">

                        <div className="col">
                            <label>Other Spcifications</label>
                            <RtfEditor updateParentState={updateParentState} />
                        </div>
                    </div>
                    <div className="row m-2">
                        <div className="col">
                            <button type="submit" className="btn btnPrimary justify-content-center">Save</button>
                        </div>
                    </div>
                </form>
            </div >
        </>
    )
}

export default SpecificationsForm
