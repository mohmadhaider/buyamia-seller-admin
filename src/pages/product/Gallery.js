import React, { useState } from 'react'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import MediaModal from './Modal/MediaModal'

const Gallery = (props) => {
    const [showModal, setShowModal] = useState(false)
    const [mediaState, setMediaState] = useState([])
    const [tempMedia, setTempMedia] = useState([])
    const [updateModal, setUpdateModal] = useState(false)

    const onEditRow = (index, media) => {
        setTempMedia(media);
        setUpdateModal(true);
    }

    const onDeleteRow = (i) => {
        // let newArray = [...state.variant];
        // newArray.splice(i, 1);
        // setState({ OrderQuantities: newArray });
    };
    let saveData = () => {
        props.func();
    }

    return (
        <div className='container'>
            <div className="row mt-5">
                <div className="col-8">
                    <p className="h3" style={{ "color": "green" }}>Media</p>
                </div>
            </div>
            <div className='row'>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Variant Image</th>
                            <th scope="col">Variant Name</th>
                            <th scope="col">Options</th>
                            <th scope="col justify-content-center">Actions</th>
                            <th scope="col"><button className='btn btnPrimary' onClick={() => setShowModal(true)}><CreateIcon color="action" /></button></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            mediaState && mediaState.map((media, index) => (
                                <tr key={index}>
                                    <td>{media.minQty}</td>
                                    <td>{media.variantType}</td>
                                    <td>{media.variantOptions}</td>

                                    <td><button className='mx-1' onClick={() => onEditRow(index, { ...media, id: index })}><EditIcon color="action" /></button>
                                        <button className='' onClick={() => {
                                            let medias = mediaState
                                            media = medias.slice(index + 1, 1)
                                            setMediaState(media)
                                            localStorage.setItem("Media", JSON.stringify(mediaState));
                                        }
                                        }><DeleteIcon color="action" /></button></td>
                                </tr>
                            ))




                        /* {state.OrderQuantities.length > 0 && state.OrderQuantities.map((d, i) => (
                        <tr key={i}>
                            <>
                                <td><input type="text" className="form-control" name="minQty" value={d.minQty} onChange={changeEvent(i, "minQty")} placeholder="Min Qty" /></td>
                                <td><input type="text" className="form-control" name="maxQty" value={d.maxQty} onChange={changeEvent(i, "maxQty")} placeholder="Max Qty" /></td>
                                <td><input type="text" className="form-control" name="price" value={d.price} onChange={changeEvent(i, "price")} placeholder="Price" /></td>
                                <td><input type="text" className="form-control" name="fromLeadtime" value={d.fromLeadtime} onChange={changeEvent(i, "fromLeadTime")} placeholder="From Leadtime" /></td>
                                <td><input type="text" className="form-control" name="toLeadtime" value={d.toLeadtime} onChange={changeEvent(i, "toLeadTime")} placeholder="To Leadtime" /></td>
                                <td><button className='mx-1' onClick={() => onEditRow(i)}><EditIcon color="action" /></button>
                                    <button className='' onClick={() => onDeleteRow(i)}><DeleteIcon color="action" /></button></td>
                            </>
                        </tr>
                    ))} */}
                    </tbody>
                </table>
            </div>
            <button onClick={() => (saveData)} className='btn btn-success my-2' >Save</button>
            {
                showModal && (
                    <MediaModal
                        show={showModal}
                        setShowModal={setShowModal} />
                )
            }
            {
                updateModal && (
                    <MediaModal
                        show={updateModal}
                        setShowModal={setShowModal}
                        updateData={tempMedia} />
                )
            }
        </div>

    )
}

export default Gallery
