//https://codepen.io/FLCcrakers/pen/JZVeZE?editors=0111

//https://codepen.io/tinymce/pen/QjzgRW

import React, { Component } from "react";
import ReactQuill from "react-quill";
import "quill-mention";
import "quill-mention/dist/quill.mention.css";
import "./style.css";
const atValues = [
  { id: 1, value: "Fredrik Sundqvist" },
  { id: 2, value: "Patrik Sjölin" }
];

const toolbarOptions = ["bold"];

class Editor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shortDescription: "",
      detailDescription: ""
    };
    this.handleProcedureContentChange = this.handleProcedureContentChange.bind(this);
  }

  modules = {
    toolbar: [
      [{ header: [1, 2, false] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [{ list: "ordered" }, { list: "bullet" }],
      ["link", "image"]
    ],
    mention: {
      allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
      mentionDenotationChars: ["@", "#"],
      source: function (searchTerm, renderItem, mentionChar) {
        let values;
        if (mentionChar === "@" || mentionChar === "#") {
          values = atValues;
        }
        if (searchTerm.length === 0) {
          renderItem(values, searchTerm);
        } else {
          const matches = [];
          for (let i = 0; i < values.length; i++)
            if (
              ~values[i].value.toLowerCase().indexOf(searchTerm.toLowerCase())
            )
              matches.push(values[i]);
          renderItem(matches, searchTerm);
        }
      }
    }
  };

  formats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "mention"
  ];
  handleProcedureContentChange = (value) => {
    console.log("e", value);
    this.setState({shortDescription: value});
    // this.setState({ ...this.state, "shortDescription": this.state.shortDescription, "detailDescription": this.state.detailDescription })
  };
  
  changeEvent = () => { }

  render() {
    return (
      <>
        <div id="root" className='container mx-0 my-3'>
          {/* <div className='my-2 font-weight-bold' style={{ color: "green" }}>Product Short Description</div> */}
          <ReactQuill
            theme="snow"
            modules={this.modules}
            formats={this.formats}
            value={this.state.shortDescription}
            onChange={this.handleProcedureContentChange}
          >
            <div className="my-editing-area" />
          </ReactQuill>
        </div>
      </>


    );
  }
}

export default Editor;
