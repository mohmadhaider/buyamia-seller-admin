import React, { useState, useEffect } from 'react'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create'
import OrderQuantitiesModal from './Modal/OrderQuantitiesModal';

const OrderQuantities = (props) => {

    let orderArr = [];
    const [showModal, setShowModal] = useState(false)
    const [updateModal, setUpdateModal] = useState(false)
    const [orderQuantities, setOrderQuantities] = useState(JSON.parse(localStorage.getItem("OrderQuantities")));
    const [tempOrder, setTempOrder] = useState();
    let openModal = () => {

        // let finalArray = [...state.OrderQuantities];
        // const newRow = {
        //     minQty: "",
        //     maxQty: "",
        //     price: "",
        //     fromLeadtime: "",
        //     toLeadtime: ""
        // };
        // finalArray.push(newRow);
        // setState({ OrderQuantities: finalArray })
    }
    // const changeEvent = (i, key) => {
    //     return (e) => {
    //         if (key === "minQty") {
    //             const value = e.target.value;
    //             let newArray = [...state.OrderQuantities];
    //             newArray[i].minQty = value;
    //             setState({
    //                 OrderQuantities: newArray
    //             });
    //         }
    //         if (key === "maxQty") {
    //             const value = e.target.value;
    //             let newArray = [...state.OrderQuantities];
    //             newArray[i].maxQty = value;
    //             setState({
    //                 OrderQuantities: newArray
    //             });
    //         }
    //         if (key === "price") {
    //             const value = e.target.value;
    //             let newArray = [...state.OrderQuantities];
    //             newArray[i].price = value;
    //             setState({
    //                 OrderQuantities: newArray
    //             });
    //         }
    //         if (key === "fromLeadTime") {
    //             const value = e.target.value;
    //             let newArray = [...state.OrderQuantities];
    //             newArray[i].fromLeadtime = value;
    //             setState({
    //                 OrderQuantities: newArray
    //             });
    //         }
    //         if (key === "toLeadTime") {
    //             const value = e.target.value;
    //             let newArray = [...state.OrderQuantities];
    //             newArray[i].toLeadtime = value;
    //             setState({
    //                 OrderQuantities: newArray
    //             });
    //         }
    //     }
    // };
    // useEffect(() => {
    //     if (localStorage.getItem("OrderQuantities")){
    //         orderArr = JSON.parse(localStorage.getItem("OrderQuantities"));
    //         console.log("orderArr",orderArr);
    //         setOrderQuantities(orderArr);
    //     }

    // }, []);

    const onEditRow = (index, order) => {
        setTempOrder(order);
        setUpdateModal(true);
    }

    const onDeleteRow = (i) => {
        // let newArray = [...state.variant];
        // newArray.splice(i, 1);
        // setState({ OrderQuantities: newArray });
    };
    let saveData = () => {
        props.func();
    }
    return (
        <div className='container'>
            <div className="row">
                <div className="col-8">
                    <p className="mt-5 h3" style={{ "color": "green" }}>Order Quantities</p>
                </div>
            </div>
            <div className='row'>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Min Qty</th>
                            <th scope="col">Max Qty</th>
                            <th scope="col">Price</th>
                            <th scope="col">Leadtime Desc</th>
                            <th scope="col justify-content-center">Actions</th>
                            <th scope="col"><button className='btn btn-success' onClick={() => setShowModal(true)}><CreateIcon color="action" /></button></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            orderQuantities && orderQuantities.map((order, index) => (
                                <tr key={index}>
                                    <td>{order.minQty}</td>
                                    <td>{order.maxQty}</td>
                                    <td>{order.price}</td>
                                    <td>{order.fromLeadtime} to {order.toLeadtime} days</td>

                                    <td><button className='mx-1' onClick={() => onEditRow(index, {...order, id: index})}><EditIcon color="action" /></button>
                                        <button className='' onClick={() => {
                                            let orders = orderQuantities
                                            order = orders.slice(index + 1, 1)
                                            setOrderQuantities(order)
                                            localStorage.setItem("OrderQuantities", JSON.stringify(orderQuantities));
                                        }
                                        }><DeleteIcon color="action" /></button></td>
                                </tr>
                            ))




                        /* {state.OrderQuantities.length > 0 && state.OrderQuantities.map((d, i) => (
                        <tr key={i}>
                            <>
                                <td><input type="text" className="form-control" name="minQty" value={d.minQty} onChange={changeEvent(i, "minQty")} placeholder="Min Qty" /></td>
                                <td><input type="text" className="form-control" name="maxQty" value={d.maxQty} onChange={changeEvent(i, "maxQty")} placeholder="Max Qty" /></td>
                                <td><input type="text" className="form-control" name="price" value={d.price} onChange={changeEvent(i, "price")} placeholder="Price" /></td>
                                <td><input type="text" className="form-control" name="fromLeadtime" value={d.fromLeadtime} onChange={changeEvent(i, "fromLeadTime")} placeholder="From Leadtime" /></td>
                                <td><input type="text" className="form-control" name="toLeadtime" value={d.toLeadtime} onChange={changeEvent(i, "toLeadTime")} placeholder="To Leadtime" /></td>
                                <td><button className='mx-1' onClick={() => onEditRow(i)}><EditIcon color="action" /></button>
                                    <button className='' onClick={() => onDeleteRow(i)}><DeleteIcon color="action" /></button></td>
                            </>
                        </tr>
                    ))} */}
                    </tbody>
                </table>
            </div>
            <button onClick={() => (saveData)} className='btn btn-success my-2' >Save</button>
            {
                showModal && (
                    <OrderQuantitiesModal
                        show={showModal}
                        setShowModal={setShowModal} />
                )
            }
            {
                updateModal && (
                    <OrderQuantitiesModal
                        show={updateModal}
                        setShowModal={setShowModal}
                        updateData={tempOrder} />
                )
            }

        </div>
    )
}

export default OrderQuantities
