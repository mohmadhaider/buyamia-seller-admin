import React, { useState } from 'react'
import DescriptionForm from './DescriptionForm'
import Gallery from './Gallery'
import OrderQuantities from './OrderQuantities'
import ProductInfoForm from './ProductInfoForm'
import SpecificationsForm from './SpecificationsForm'
import VariantForm from './VariantForm'



const Product = () => {
    const [changePage, setChangePage] = useState(   {
        Product: true,
        Order: false,
        Variants: false,
        Media: false,
        specifications: false

    })
    let onInformation = () => {
        setChangePage(
            {
                Product: true,
                Order: false,
                Variants: false,
                Media: false,
                specifications: false
        
            }
        )
    }
    let onOrder = () => {
        setChangePage(
            {
                Product: false,
                Order: true,
                Variants: false,
                Media: false,
                specifications: false
        
            }
        )
    }
    let onvariants = () => {
        setChangePage(
            {
                Product: false,
                Order: false,
                Variants: true,
                Media: false,
                specifications: false
        
            }
        )
    }
    let onSpecifications = () => {
        setChangePage(
            {
                Product: false,
                Order: false,
                Variants: false,
                Media: false,
                specifications: true
        
            }
        )
    }
    let onGalery = () => {
        setChangePage(
            {
                Product: false,
                Order: false,
                Variants: false,
                Media: true,
                specifications: false
        

            }
        )
    }
    return (
        <>
            <div className='container'>
                <div className='row justify-content-center'>
                    <ul class="nav">
                        <li class="nav-item">
                            <button class="nav-link active" onClick={onInformation} >Product Details</button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link active" onClick={onOrder} >Order Quantities</button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link active" onClick={onvariants} >Variants</button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link active" onClick={onSpecifications} >Specifications</button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link active" onClick={onGalery} >Media</button>
                        </li>
                       
                    </ul>
                </div>
                {
                    changePage.Product &&
                    <ProductInfoForm func={onOrder} />
                }
                {/* {
                    changePage.Description &&
                    <DescriptionForm func={i=onvariants} />
                } */
                    changePage.Order &&
                    <OrderQuantities func={onvariants} />
                }
                {
                    changePage.Variants &&
                    <VariantForm func={onSpecifications} />
                }
                 {
                    changePage.specifications &&
                    <SpecificationsForm />
                }
                 {
                    changePage.Media &&
                    <Gallery />
                }
                {/* {
                    changePage.M &&
                    <ProductInfoForm func={]onGalery} />
                } */}
               
            </div>
        </>
    )
}

export default Product
