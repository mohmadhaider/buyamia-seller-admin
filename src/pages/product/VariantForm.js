import React, { useReducer, useState } from 'react';
import { styled } from '@mui/material/styles';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import VariantModal from './Modal/VariantModal';

const IOSSwitch = styled((props) => (
    <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,
        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(16px)',
            color: '#fff',
            '& + .MuiSwitch-track': {
                backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
                opacity: 1,
                border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
                opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: '#33cf4d',
            border: '6px solid #fff',
        },
        '&.Mui-disabled .MuiSwitch-thumb': {
            color:
                theme.palette.mode === 'light'
                    ? theme.palette.grey[100]
                    : theme.palette.grey[600],
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
    },
    '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 22,
        height: 22,
    },
    '& .MuiSwitch-track': {
        borderRadius: 26 / 2,
        backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
    },
}));

const VariantForm = () => {

    const [showModal, setShowModal] = useState(false)
    const [updateModal, setUpdateModal] = useState(false)
    const [tempVariant, setTempVariant] = useState();
    const [variantState, setVariantState] = useState(JSON.parse(localStorage.getItem("Variant")))


    const onEditRow = (i) => { }

    // const onDeleteRow = (i) => {
    //     let newArray = [...state.variant];
    //     newArray.splice(i, 1);
    //     setState({ variant: newArray });
    // };

    const saveData = () => { }

    return (
        <div className='container'>
            <div className="row">
                <div className="col-8">
                <p className="mt-5 h3" style={{ "color": "green" }}>Variant</p>
                </div>
            </div>
            <div className='row'>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Product Id</th>
                            <th scope="col">Variant Name</th>
                            <th scope="col">Variant Options</th>
                            <th scope="col justify-content-center">Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                        {/* {
                            variantState && variantState.map((variant, index) => (
                                <tr key={index}>
                                    <td>{variant.variantname}</td>
                                    <td>{variant.options}</td>

                                    <td><button className='mx-1' onClick={() => onEditRow(index, { ...variant, id: index })}><EditIcon color="action" /></button>
                                        <button className='' onClick={() => {
                                            let Variants = variantState
                                            variant = Variants.slice(index + 1, 1)
                                            setVariantState(variant)
                                            localStorage.setItem("Variant", JSON.stringify(variantState));
                                        }
                                        }><DeleteIcon color="action" /></button></td>
                                </tr>
                            ))
                        } */}
                         <tr>
                            <td scope="col">1</td>
                            <td scope="col"><input type="text" className="form-control" placeholder='Enter Color'/></td>
                            <td scope="col"><input type="text" className="form-control" placeholder='Options Comma seprated (Red,Green)'/></td>
                            <td><button className='mx-1' onClick={() => {} }><EditIcon color="action" /></button>
                                        <button className='' onClick={() => {} }><DeleteIcon color="action" /></button></td>
                        </tr>
                        <tr>
                            <td scope="col">1</td>
                            <td scope="col"><input type="text" className="form-control" placeholder='Enter Material'/></td>
                            <td scope="col"><input type="text" className="form-control" placeholder='Options Comma seprated (Teak,Wood,Steal)'/></td>
                            <td><button className='mx-1' onClick={() => {} }><EditIcon color="action" /></button>
                                        <button className='' onClick={() => {} }><DeleteIcon color="action" /></button></td>
                        </tr>
                        <tr>
                            <td scope="col">1</td>
                            <td scope="col"><input type="text" className="form-control" placeholder='Enter Size'/></td>
                            <td scope="col"><input type="text" className="form-control" placeholder='Options Comma seprated (S,M,XL.XXL)'/></td>
                            <td><button className='mx-1' onClick={() => {} }><EditIcon color="action" /></button>
                                        <button className='' onClick={() => {} }><DeleteIcon color="action" /></button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <button onClick={() => (saveData)} className='btn btn-success my-2' >Save</button>
            {
                showModal && (
                    <VariantModal
                        show={showModal}
                        setShowModal={setShowModal} />
                )
            }
            {
                updateModal && (
                    <VariantModal
                        show={updateModal}
                        setShowModal={setShowModal}
                        updateData={tempVariant} />
                )
            }

        </div>
    )
}

export default VariantForm
