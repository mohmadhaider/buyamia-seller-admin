import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import {
  Col,
  Row,
  Progress,
  Button,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Container,
} from "reactstrap";

import LinkCard from "./components/LinkCard";
import Setup from "../../assets/images/Setup.png";
import Price from "../../assets/images/Price.png";
import Logistic from "../../assets/images/Logistic.png";
import Collection from "../../assets/images/Collection.png";
import Payouts from "../../assets/images/Payouts.png";
import Messaging from "../../assets/images/Messaging.png";
// import ControlAndMonitoring from "../../assets/images/Control&Monitoring.jpg";

const Home = () => {
  return (
    <div>
      <Row>
        <Col lg="4" md="4" sm="12" xs="12">
          <LinkCard linkName="Product" cardImage={Setup} linkUrl="/product" />
        </Col>
        <Col lg="4" md="4" sm="12" xs="12">
          <LinkCard linkName="Price" cardImage={Price} linkUrl="" />
        </Col>
        <Col lg="4" md="4" sm="12" xs="12">
          <LinkCard linkName="Logistic" cardImage={Logistic} linkUrl="" />
        </Col>
        <Col lg="4" md="4" sm="12" xs="12">
          <LinkCard linkName="Collections" cardImage={Collection} linkUrl="" />
        </Col>
        <Col lg="4" md="4" sm="12" xs="12">
          <LinkCard linkName="Pay Outs" cardImage={Payouts} linkUrl="" />
        </Col>
        <Col lg="4" md="4" sm="12" xs="12">
          <LinkCard linkName="Media & Messaging" cardImage={Messaging} linkUrl="" />
        </Col>
      </Row>
    </div>
  );
};

export default Home;
