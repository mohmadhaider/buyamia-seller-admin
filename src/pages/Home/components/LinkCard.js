import React from 'react'
import { Link } from 'react-router-dom';
import { Card, CardBody } from 'reactstrap'
import s from './LinkCard.module.scss'
const LinkCard = ({cardImage, linkName,linkUrl}) => {
    return (
      <>
        <Link to={linkUrl}>
          <Card className={s.card}>
            <CardBody className={s.cardbody}>
              <img src={cardImage}></img>
              <span>{linkName}</span>
            </CardBody>
          </Card>
        </Link>
      </>
    );
}

export default LinkCard
