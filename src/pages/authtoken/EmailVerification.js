import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom'
import { AuthTokenService } from '../../services/AuthTokenService';

const EmailVerification = () => {
    const { hasAuthToken } = AuthTokenService();
    const { token } = useParams();
   
    async function checkToken() {
        if (token) {
            let responseData = await hasAuthToken({"token":token});
            localStorage.setItem("AuthToken",token);
            localStorage.setItem("PortalUserData",responseData);
        }
    }
    useEffect(() => {
        checkToken();
    }, [])
    return (
        <div>
            {/* <h3>token : {token}</h3> */}
        </div>
    )
}

export default EmailVerification
