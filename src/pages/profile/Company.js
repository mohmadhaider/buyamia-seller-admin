import React, { useState } from 'react';
import { styled } from '@mui/material/styles';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';


const IOSSwitch = styled((props) => (
    <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,
        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(16px)',
            color: '#fff',
            '& + .MuiSwitch-track': {
                backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
                opacity: 1,
                border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
                opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: '#33cf4d',
            border: '6px solid #fff',
        },
        '&.Mui-disabled .MuiSwitch-thumb': {
            color:
                theme.palette.mode === 'light'
                    ? theme.palette.grey[100]
                    : theme.palette.grey[600],
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
    },
    '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 22,
        height: 22,
    },
    '& .MuiSwitch-track': {
        borderRadius: 26 / 2,
        backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
    },
}));

const Company = (props) => {
    const [profile, setProfile] = useState(JSON.parse(localStorage.getItem("ProfileDetails")));

    let changeEvent = (e) => {
        setProfile({ ...profile, [e.target.name]: e.target.value })
    }

    let saveData = () => {
        localStorage.removeItem("ProfileDetails");
        localStorage.setItem("ProfileDetails",JSON.stringify(profile));
        props.func();
     }

    return (
        <div className='container'>
            <form onSubmit={saveData}>
                <p className='justify-content-center h2 px-4 my-3'>Company Details</p>
                <div className="row m-2" >
                    <div className="col">
                        <input type="text" className="form-control" name="bizName" value={profile.bizName} onChange={(e) => { changeEvent(e) }} placeholder="Biz name" />
                    </div>
                    <div className="col">
                        <input type="text" className="form-control" name="bizLic" value={profile.bizLic} onChange={(e) => { changeEvent(e) }} placeholder="Biz Lic / Tax ID/ Reseller Lic" />
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <input type="text" className="form-control" name="factoryArea" value={profile.factoryArea} onChange={(e) => { changeEvent(e) }} placeholder="Factory Area Sq/Ft" />
                    </div>
                    <div className="col">
                        <input type="text" className="form-control" name="wareHouseArea" value={profile.wareHouseArea} onChange={(e) => { changeEvent(e) }} placeholder="Warehouse Area Sq/Ft" />
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <input type="text" className="form-control" name="noOfWorkers" value={profile.noOfWorkers} onChange={(e) => { changeEvent(e) }} placeholder="No of Workers" />
                    </div>
                    <div className="col">
                        <input type="text" className="form-control" name="productionCapacity" value={profile.productionCapacity} onChange={(e) => { changeEvent(e) }} placeholder="Production Capacity Per/Day" />
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <div className="input-group">
                            <select className="custom-select" name="item" value={profile.item} onChange={(e) => { changeEvent(e) }} id="inputGroupSelect04">
                                <option selected>Choose...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                    </div>
                    <div className="col">
                        <div className='row'>
                            <div className='col'> <label className='font-weight-bold'>Reg Start Date</label></div>
                            <div className='col'><input type="date" name="regDate" value={profile.regDate} onChange={(e) => { changeEvent(e) }} className="form-control" /></div>
                        </div>
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <FormControlLabel name="export" value={profile.export} onChange={(e) => { changeEvent(e) }}
                            control={<IOSSwitch sx={{ m: 1 }}  />}
                            label="Export"
                        />
                    </div>
                    <div className="col">
                        <FormControlLabel name="packing" value={profile.packing} onChange={(e) => { changeEvent(e) }}
                            control={<IOSSwitch sx={{ m: 1 }}  />}
                            label="Packing"
                        />
                    </div>
                    <div className="col">
                        <FormControlLabel name="inHouseQC" value={profile.inHouseQC} onChange={(e) => { changeEvent(e) }}
                            control={<IOSSwitch sx={{ m: 1 }}  />}
                            label="In House QC"
                        />
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <FormControlLabel name="whiteLabeling" value={profile.whiteLabeling} onChange={(e) => { changeEvent(e) }}
                            control={<IOSSwitch sx={{ m: 1 }}  />}
                            label="White Labeling"
                        />
                    </div>
                    <div className="col">
                        <FormControlLabel name="shipping" value={profile.shipping} onChange={(e) => { changeEvent(e) }}
                            control={<IOSSwitch sx={{ m: 1 }}  />}
                            label="Shipping"
                        />
                    </div>
                    <div className="col">
                        <FormControlLabel name="cusomization" value={profile.cusomization} onChange={(e) => { changeEvent(e) }}
                            control={<IOSSwitch sx={{ m: 1 }}  />}
                            label="Cusomization"
                        />
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <div className="form-floating">
                            <label className='font-weight-bold'>About Company</label>
                            <textarea className="form-control" name="aboutCompany" value={profile.aboutCompany} onChange={(e) => { changeEvent(e) }} placeholder=" Write Here About Company" id="floatingTextarea2" style={{ height: "100px" }}></textarea>
                        </div>
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <button type="text" className="btn btn-success justify-content-center">Save</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default Company
