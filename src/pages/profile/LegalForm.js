import React, { useState } from 'react';
import { styled } from '@mui/material/styles';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import { handleBreakpoints } from '@mui/system';

const IOSSwitch = styled((props) => (
    <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,
        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(16px)',
            color: '#fff',
            '& + .MuiSwitch-track': {
                backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
                opacity: 1,
                border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
                opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: '#33cf4d',
            border: '6px solid #fff',
        },
        '&.Mui-disabled .MuiSwitch-thumb': {
            color:
                theme.palette.mode === 'light'
                    ? theme.palette.grey[100]
                    : theme.palette.grey[600],
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
    },
    '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 22,
        height: 22,
    },
    '& .MuiSwitch-track': {
        borderRadius: 26 / 2,
        backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
    },
}));


const LegalForm = (props) => {
    const profileDetails = JSON.parse(localStorage.getItem("ProfileDetails"))
    const [isExportOption, setIsExportOption] = useState(false)
    const [profile, setProfile] = useState(profileDetails);

    let handleChange = (e) => {
        setIsExportOption(e.target.checked);
        setProfile({ ...profile, export: e.target.checked });
    }

    let changeEvent = (e) => {
        setProfile({ ...profile, [e.target.name]: e.target.value })
    }
    let saveData = () => {
        setProfile({ ...profile, export: isExportOption });
        localStorage.removeItem("ProfileDetails");
        localStorage.setItem("ProfileDetails", JSON.stringify(profile));
        props.func();
    }

    return (
        <div className='container'>
            <form onSubmit={saveData}>
                <p className='justify-content-center h2 px-4 my-3'>Legal Details</p>
                <div className="row m-2" >
                    <div className="col">
                        <label className='h6'>License#</label>
                        <input type="text" className="form-control" name='licensedetails' value={profile?.licensedetails} onChange={(e) => { changeEvent(e) }} placeholder="Company Registration#, Biz License#, Permit#, NIB etc" />
                    </div>
                    <div className="col">
                        <label className='h6'>Tax Id</label>
                        <input type="text" className="form-control" name='taxid' value={profile?.taxid} onChange={(e) => { changeEvent(e) }} placeholder="Enter Your Tax Id" />
                    </div>
                </div>
                <div className="row m-2">
                    <div className="mt-3 col-6">
                        <FormControlLabel name="export" value={profile?.export}
                            checked={isExportOption}
                            onChange={(e) => { handleChange(e) }}
                            control={<IOSSwitch sx={{ m: 1 }} />}
                            label="Export"
                        />
                    </div>
                    <div className="col-6">
                        {
                            isExportOption &&
                            <div>
                                <label className='h6'>Export License#</label>
                                <input type="text" className="form-control" name='exportdetails' value={profile?.exportdetails} onChange={(e) => { changeEvent(e) }} placeholder="Export License# or Passport#" />
                            </div>}
                    </div>
                </div>
                <div className="row my-2">
                    <div className="mt-3 col">
                        <div className="col">
                            <label className='h6'>Bank Details</label>
                            {/* <input type="text" className="form-control" name='exportdetails' value={profile?.exportdetails} onChange={(e) => { changeEvent(e) }} placeholder="Export Lic# or Passport#" /> */}
                            <textarea className="form-control" name="bankdetails" value={profile?.bankdetails} onChange={(e) => { changeEvent(e) }} rows="3" placeholder={`Account Number in which you want to receive money
Account Holder Name
Bank Name
Wire Trf Code`}></textarea>
                        </div>
                    </div>
                </div>
                <div className="row my-2">
                    <div className="mt-3 col">
                        <div className="col">
                            <label className='h6'>Certificates</label>
                            <textarea className="form-control" name="Certificates" value={profile?.certificates} onChange={(e) => { changeEvent(e) }} rows="2" placeholder="Enter your Certificates details"></textarea>
                        </div>
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <button type="submit" className="btn btnPrimary justify-content-center">Save</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default LegalForm
