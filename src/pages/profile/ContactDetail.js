import React, { useState, useEffect } from "react";
import Axios from "axios";
const ContactDetail = (props) => {
  const [country, setCountry] = useState([]);
  const [city, setCity] = useState([]);
  const [state, setState] = useState([]);
  const [selectCountry, setSelectCountry] = useState("");
  const [selectState, setSelectState] = useState("");
  const [selectCity, setSelectCity] = useState("");
  useEffect(() => {
    Axios.get("https://countriesnow.space/api/v0.1/countries/capital").then(
      (data) => {
        setCountry(data.data.data);
      }
    );
  }, []);

  const onChangeCountry = (e) => {
    setState([]);
    setSelectCountry(e.target.value);
    Axios.post("https://countriesnow.space/api/v0.1/countries/states", {
      country: e.target.value,
    }).then((data) => {
      //   console.log(data.data.data.);
      setState(data.data.data.states);
    });
  };
  const onChangeState = (e) => {
    setCity([]);
    setSelectState(e.target.value);
    Axios.post("https://countriesnow.space/api/v0.1/countries/state/cities", {
      country: selectCountry,
      state: e.target.value,
    }).then((data) => {
      //   console.log(data.data.data);
      setCity(data.data.data);
    });
  };

  const onChangeCity = (e) => {
    setSelectCity(e.target.value);
  };
  const profileDetails = JSON.parse(localStorage.getItem("ProfileDetails"));
  const [Profile, setProfile] = useState(profileDetails);

  let changeEvent = (event) => {
    setProfile({ ...Profile, [event.target.name]: event.target.value });
  };
  let saveData = () => {
    console.log(selectCountry, selectState, selectCity);
    localStorage.removeItem("ProfileDetails");
    localStorage.setItem("ProfileDetails", JSON.stringify(Profile));
    props.func();
  };
  return (
    <div className="container">
      <form onSubmit={saveData}>
        <p className="justify-content-center h2 px-4 my-3">Contact Details</p>
        <div className="row m-2">
          <div className="col">
            <label className="h6">Contact Person Name</label>

            <input
              type="text"
              className="form-control"
              required
              name="contactpersonname"
              value={Profile?.contactpersonname}
              onChange={(e) => {
                changeEvent(e);
              }}
              placeholder="Enter  Contact Person Name"
            />
          </div>
          <div className="col">
            <label className="h6">Email Id</label>
            <input
              type="email"
              className="form-control"
              required
              name="emailid"
              value={Profile?.emailid}
              onChange={(e) => {
                changeEvent(e);
              }}
              placeholder="Enter Your Email Id"
            />
          </div>
        </div>
        <div className="row m-2">
          <div className="col">
            <label className="h6">Phone 1</label>
            <input
              type="text"
              className="form-control"
              required
              name="phone1"
              value={Profile?.phone1}
              onChange={(e) => {
                changeEvent(e);
              }}
              placeholder="Enter Your Phone 1"
            />
          </div>
          <div className="col">
            <label className="h6">Phone 2</label>
            <input
              type="text"
              className="form-control"
              required
              name="phone2"
              value={Profile?.phone2}
              onChange={(e) => {
                changeEvent(e);
              }}
              placeholder="Enter Your Phone 2"
            />
          </div>
        </div>
        <div className="row m-2">
          <div className="col-6">
            <label className="h6">Country</label>
            <select
              class="form-control"
              name="selectCountry"
              value={selectCountry}
              onChange={onChangeCountry}
            >
              <option value="0" selected>
                Select Country
              </option>
              {country?.map((tempCountry) => (
                <option value={tempCountry.name}>{tempCountry.name}</option>
              ))}
            </select>
          </div>
          <div className="col">
            <label className="h6">Address</label>
            <input
              type="text"
              className="form-control"
              name="address"
              value={Profile?.address}
              onChange={(e) => {
                changeEvent(e);
              }}
              placeholder="Enter Your Address"
            />
          </div>

        </div>
        <div className="row m-2">
          <div className="col-6">
            <label className="h6">City</label>
            <select
              class="form-control"
              name="selectCountry"
              value={selectCity}
              onChange={onChangeCity}
            >
              <option value="0" selected>
                Select City
              </option>
              {city?.map((tempCity) => (
                <option value={tempCity}>{tempCity}</option>
              ))}
            </select>
          </div>
          <div className="col">
            <label className="h6">State</label>
            <select
              class="form-control"
              name="selectCountry"
              value={selectState}
              onChange={onChangeState}
            >
              <option value="0" selected>
                Select State
              </option>
              {state?.map((tempState) => (
                <option value={tempState.name}>{tempState.name}</option>
              ))}
            </select>
          </div>
        </div>

        <div className="row m-2">
          <div className="col-3">
            <label className="h6">Postal Code</label>
            <input
              type="text"
              className="form-control"
              name="zipcode"
              value={Profile?.zipcode}
              onChange={(e) => {
                changeEvent(e);
              }}
              placeholder="Enter Your Postal Code"
            />
          </div>
        </div>
        <div className="row m-2">
          <div className="col">
            <button
              type="submit"
              className="btn btnPrimary justify-content-center"
            >
              Save
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default ContactDetail;