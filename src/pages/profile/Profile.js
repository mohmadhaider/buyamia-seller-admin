import React, { useState, useEffect } from 'react'
import { useProfileData } from '../../services/ProfileService'
import BusinessForm from './BusinessForm'
import Company from './Company'
import ContactDetail from './ContactDetail'
import LegalForm from './LegalForm'
import ProfileForm from './ProfileForm'


const Profile = () => {

    const { GetSellerProfile, InsertSeller,GetBusinessCategory } = useProfileData();

    const [profileObj, setProfileObj] = useState(localStorage.getItem("ProfileDetails") ? JSON.parse(localStorage.getItem("ProfileDetails")) : null)
    const [activeButton, setActiveButton] = useState({
        profile: true,
        legal: false,
        company: false,
        business: false
    })
    const [sellerData, setSellerData] = useState(null)
    const [businessCategory, setBusinessCategory] = useState(null)

    const navSize = {
        "height": "100%",
        "width": "20%",
        "border": "1px",
        "margin-left": "35px"
    }
    const mainSize = {
        "height": "100%",
        "width": "80%"
    }

    const [changePage, setChangePage] = useState({
        profile: true,
        legal: false,
        company: false,
        business: false
    });


    let ChangeProfile = () => {
        setChangePage({
            profile: true,
            legal: false,
            contact: false,
            business: false
        })
    }
    let ChangeLegal = () => {
        setChangePage({
            profile: false,
            legal: true,
            contact: false,
            business: false
        })
    }
    let ChangeBusinessDetails = () => {
        setChangePage({
            profile: false,
            legal: false,
            contact: false,
            business: true
        })
    }
    let ChangeContactDetail = () => {
        setChangePage({
            profile: false,
            legal: false,
            contact: true,
            business: false
        })
    }



    let saveDetails = async () => {
        if(profileObj)
        {
            // setProfileObj({...profileObj,createdby:1})
            const SellerData = await InsertSeller(profileObj)
        }
        
    }

    async function GetSellerData(){
        let finalarray =[];
        let BusinessCategoryData = await GetBusinessCategory()
        BusinessCategoryData.data.filter((category)=>{
            if(category.hassubcategory){
                finalarray.push(category);
                setBusinessCategory(finalarray);
            }
        })
        const SellerData = await GetSellerProfile(3);
        setSellerData(SellerData.data);
    }

    useEffect(() => {
        GetSellerData();
    }, []);


    return (
        <div className='container'>
            <div className='row justify-content-center'>
                <ul className="nav">
                    <li className="nav-item">
                        <button className={setChangePage.profile ? "nav-link active" : "nav-link"} onClick={ChangeProfile} >Profile</button>
                    </li>
                    <li className="nav-item">
                        <button className={setChangePage.contact ? "nav-link active" : "nav-link"} onClick={ChangeContactDetail} >Contact Detail</button>
                    </li>
                    <li className="nav-item">
                        <button className={setChangePage.legal ? "nav-link active" : "nav-link"} onClick={ChangeLegal} >Legal Details</button>
                    </li>

                    <li className="nav-item">
                        <button className={setChangePage.business ? "nav-link active" : "nav-link"} onClick={ChangeBusinessDetails}>Business Details</button>
                    </li>
                </ul>
            </div>
            <button type='button' className='float right btn btn-success' onClick={saveDetails}>Save Details</button>
            <div className='row'>
                <div className='col' style={mainSize}>
                    {
                        changePage.profile &&
                        <ProfileForm func={ChangeContactDetail} sellerData = {sellerData} setSellerData={setSellerData} businessData={businessCategory} />
                    }
                    {
                        changePage.contact &&
                        <ContactDetail func={ChangeLegal} />
                    }
                    {
                        changePage.legal &&
                        <LegalForm func={ChangeBusinessDetails} />
                    }
                    {
                        changePage.business &&
                        <BusinessForm />
                    }
                </div>
            </div>
        </div>
    )
}

export default Profile


