import React, { useState, useEffect } from 'react'

// - Store Display Name
// - Company Name
// - Company Type (LLP, Prop, Partner, Ltd etc)
// - Type (Manufacturer/Trader/Agent)
// - Business Type (Biz category)
// - About Company (big box of 4 lines)

const ProfileForm = (props) => {
    const [Profile, setProfile] = useState({
        storename: "",
        businessname: "",
        companytype: "",
        sellertype: "T",
        businesscategory: "",
        aboutcompany: ""
    });

    let handleEvent = (e) => {
        setProfile({ ...Profile, [e.target.name]: e.target.value });

    }

    let changeEvent = (event, value) => {
        setProfile({ ...Profile, [event.target.name]: value });
    }
    let saveData = () => {
        localStorage.setItem("ProfileDetails", JSON.stringify(Profile));
        props.func();
    }

    useEffect(() => {
    }, [])
    return (
        <div className='container'>
            <form onSubmit={saveData}>
                <p className='justify-content-center h2 px-4 my-3'>Profile</p>
                <div className="row m-2" >
                    <div className="col">
                        <label className='h6'>Company Name</label>
                        <input type="text" className="form-control" name='businessname' value={Profile?.businessname} onChange={(e) => { handleEvent(e) }} placeholder="Enter Your Company Name" />
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col-6">
                        <label className='h6'>Company Type</label>

                        {/* <input type="text" className="form-control" name='companyType' value={Profile.companyType} onChange={(e)=>{changeEvent(e)}} placeholder="Company Type" /> */}
                        <select class="form-control" name='companytype' value={Profile?.companytype} onChange={(e) => { handleEvent(e) }}>
                            <option value="0" selected>Select Company Type</option>
                            <option value="Firma (Fa)">Firma (Fa)</option>
                            <option value="Commanditaire Vennotschap (CV)">Commanditaire Vennotschap (CV)</option>
                            <option value="Limited Company(PT)">Limited Company(PT)</option>
                            <option value="Public Company (Perum-Perusahaan Umum)">Public Company (Perum-Perusahaan Umum)</option>
                            <option value="Liability Company (Persero)">Liability Company (Persero)</option>
                        </select>
                    </div>
                    <div className='col-6'>
                        <div className='row mx-1 font-weight-bold mt-2'>Seller Type</div>
                        <div className='row'>
                            <div className="mt-2 col-4">
                                {/* <input type="text" className="form-control" name='Type' value={Profile.Type} onChange={(e)=>{changeEvent(e)}} placeholder="Type (Manufacturer/Trader/Agent)" /> */}
                                {/* <select class="form-control" name='Type' value={Profile?.Type} onChange={(e) => { changeEvent(e) }}>
                            <option selected>Select Type</option>
                            <option>Manufacturer</option>
                            <option>Trader</option>
                            <option>Agent</option>
                        </select> */}
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                        name="sellertype" id="exampleRadios3"
                                        onChange={(e) => { changeEvent(e, "M") }}
                                        checked={Profile?.sellertype == "M"}
                                        value={Profile?.sellertype} />
                                    <label class="form-check-label h6" for="exampleRadios3">
                                        Manufacturer
                                    </label>
                                </div>
                            </div>
                            <div className="mt-2 col-4">
                                <div class="form-check">
                                    <input class="form-check-input"
                                        type="radio" name="sellertype"
                                        id="exampleRadios1"
                                        checked={Profile?.sellertype == "T"}
                                        onChange={(e) => { changeEvent(e, "T") }}
                                        value={Profile?.sellertype} />
                                    <label class="form-check-label h6" for="exampleRadios1">
                                        Trader
                                    </label>
                                </div>
                            </div>
                            <div className="mt-2 col-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                        name="sellertype" id="exampleRadios2"
                                        checked={Profile?.sellertype == "A"}
                                        onChange={(e) => { changeEvent(e, "A") }}
                                        value={Profile?.sellertype} />
                                    <label class="form-check-label h6" for="exampleRadios2">
                                        Agent
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col-6">
                        <label className='h6'>Business Type</label>

                        {/* <input type="text" className="form-control" name='businessType' value={Profile.businessType} onChange={(e) => { changeEvent(e) }} placeholder="Business Type" /> */}
                        <select class="form-control" name='businesscategory' value={Profile?.businesscategory} onChange={(e) => { setProfile({ ...Profile, [e.target.name]: parseInt(e.target.value) }) }}>
                            <option value="0" selected>Select Business Type</option>
                            {

                                props.businessData && props.businessData.map((category) => (
                                    <option value={category.categoryid}>{category.name}</option>
                                ))
                            }
                            {/* <option>Manufacturer</option>
                            <option>Trader</option>
                            <option>Agent</option> */}
                        </select>
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <label className='h6'>About Company</label>
                        <textarea class="form-control" placeholder='Enter Your About Company' name='aboutcompany' value={Profile?.aboutcompany} onChange={(e) => { handleEvent(e) }} id="exampleFormControlTextarea1" rows="4"></textarea>
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <button type="submit" className="btn btnPrimary justify-content-center">Save</button>
                    </div>
                </div>
            </form>
        </div>
    )
}


// "sellertype":"M",
// "companytype":"Ltd",
// "businesscategory":1,
// "storename":"Abdulla",
// "contactpersonname":"Abdulla",
// "phone1":"1234566",
// "phone2":"1341561",
// "emailid":"abdulla@gmail.com",
// "address":"vyara",
// "zipcode":394650,
// "city":"vyara",
// "state":"Gujarat",
// "country":"India",
// "aboutcompany":"This is good company",
// "businessname":"xyz",
// "taxid":"12316",
// "licensedetails":"2131561",
// "bankdetails":"6156165",
// "export":true,
// "exportdetails":"21315613",
// "packing":true,
// "inhouseqc":false,
// "whitelabel":true,
// "customizing":false,
// "shipping":false,
// "mfgcapacity":"1500 per day",
// "factoryarea":"5000 sqrt",
// "warehousearea":"2500 sqrt",
// "numberofworkers":25000,
// "companyvideos":"",
// "companyimages":"",
// "rejectreason":"",
// "createdby":"1",
// "createddate":"2021-12-27 17:16:00",
// "updatedby":"1",
// "updateddate":"2021-12-27 17:16:00"

export default ProfileForm
