import React, { useState } from 'react'
import { styled } from '@mui/material/styles';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import { useHistory } from 'react-router-dom';
import Axios from "axios";

const IOSSwitch = styled((props) => (
    <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,
        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(16px)',
            color: '#fff',
            '& + .MuiSwitch-track': {
                backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
                opacity: 1,
                border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
                opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: '#33cf4d',
            border: '6px solid #fff',
        },
        '&.Mui-disabled .MuiSwitch-thumb': {
            color:
                theme.palette.mode === 'light'
                    ? theme.palette.grey[100]
                    : theme.palette.grey[600],
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
    },
    '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 22,
        height: 22,
    },
    '& .MuiSwitch-track': {
        borderRadius: 26 / 2,
        backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
    },
}));


const BusinessForm = (props) => {

    let history = useHistory();
    const profileDetails = JSON.parse(localStorage.getItem("ProfileDetails"))
    const [profile, setProfile] = useState(profileDetails)
    // const [inHouseQc, setInHouseQc] = useState(false)
    // const [packing, setPacking] = useState(false)
    // const [whiteLabeling, setWhiteLabeling] = useState(false)
    // const [customizing, setCustomizing] = useState(false)
    // const [shipping, setShipping] = useState(false)
    // const [vettingDone, setVettingDone] = useState(false)
    const [busineses, setBusinesses] = useState({
        inhouseqc: false,
        packing: false,
        whitelabel: false,
        customizing: false,
        shipping: false,
    });

    let handleChange = (e, key) => {
        setBusinesses({ ...busineses, [key]: e.target.checked });
    }

    let ChangeEvent = (e) => {
        setProfile({ ...profile, [e.target.name]: e.target.value });
     }


    let saveData = () => {
        let data = {...profile, ...busineses };
        localStorage.removeItem("ProfileDetails");
        localStorage.setItem("ProfileDetails", JSON.stringify(data));
    }
    return (
        <div className='container'>
            <form onSubmit={saveData}>
                <p className='justify-content-center h2 px-4 my-3'>Business Details</p>
                <div className="row m-2">
                    <div className="col-4">
                        <div className="col">
                            <FormControlLabel name="packing" value={busineses.packing}
                                checked={busineses.packing}
                                onChange={(e) => { handleChange(e, "packing") }}
                                control={<IOSSwitch sx={{ m: 1 }} />}
                                label="Packing"
                            />
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="col">
                            <FormControlLabel name="inhouseqc" value={busineses.inhouseqc}
                                checked={busineses.inhouseqc}
                                onChange={(e) => { handleChange(e, "inhouseqc") }}
                                control={<IOSSwitch sx={{ m: 1 }} />}
                                label="In House QC"
                            />
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="col">
                            <FormControlLabel name="whitelabel" value={busineses.whitelabel}
                                checked={busineses.whitelabel}
                                onChange={(e) => { handleChange(e, "whitelabel") }}
                                control={<IOSSwitch sx={{ m: 1 }} />}
                                label="White Labeling"
                            />
                        </div>
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col-4">
                        <div className="col">
                            <FormControlLabel name="customizing" value={busineses.customizing}
                                checked={busineses.customizing}
                                onChange={(e) => { handleChange(e, "customizing") }}
                                control={<IOSSwitch sx={{ m: 1 }} />}
                                label="Customizing"
                            />
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="col">
                            <FormControlLabel name="shipping" value={busineses.shipping}
                                checked={busineses.shipping}
                                onChange={(e) => { handleChange(e, "shipping") }}
                                control={<IOSSwitch sx={{ m: 1 }} />}
                                label="Shipping"
                            />
                        </div>
                    </div>
                </div>
                {/* [mfgcapacity] [nvarchar](50) NULL,
	[factoryarea] [nvarchar](50) NULL,
	[warehousearea] [nvarchar](50) NULL,
	[numberofworkers] [int] NULL, */}
                <div className="row m-2">
                    <div className="col">
                        <label className='h6'>Manufacturing Capacity</label>
                        <input type="text" className="form-control" name='mfgcapacity' value={profile?.mfgcapacity} onChange={(e) => { ChangeEvent(e) }} placeholder="Manufacturing Capacity (e.g Pcs per Month of main product)" />
                    </div>
                    <div className="col">
                        <label className='h6'>Factory Area</label>
                        <input type="text" className="form-control" name='factoryarea' value={profile?.factoryarea} onChange={(e) => { ChangeEvent(e) }} placeholder="Enter Your Factory Area (In sqrt)" />
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <label className='h6'>Warehouse Area</label>
                        <input type="text" className="form-control" name='warehousearea' value={profile?.warehousearea} onChange={(e) => { ChangeEvent(e) }} placeholder="Enter Your Warehouse Area (In sqrt)" />
                    </div>
                    <div className="col">
                        <label className='h6'>Number Of Workers</label>
                        <input type="number" className="form-control" name='numberofworkers' value={profile?.numberofworkers} onChange={(e) => { ChangeEvent(e) }} placeholder="Enter Your Number Of Workers" />
                    </div>
                </div>
                <div className="row m-2">
                    <div className="col">
                        <button type="submit" className="btn btnPrimary justify-content-center">Save</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default BusinessForm
