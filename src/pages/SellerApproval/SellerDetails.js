import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import {
  Card,
  CardHeader,
  CardBody,
  Badge,
  Row,
  Col,
  Button,
  FormGroup,
  Input,
} from "reactstrap";
import { useSellerData } from "../../services/sellerService";
import VideoSlider from "./VideoSlider";

const SellerDetails = () => {
  const history = useHistory();
  const [sellerData, setSellerData] = useState(null);
  const { id } = useParams();
  const { GetSellerDetailBySellerId } = useSellerData();
  async function GetSellerDetail() {
    const response = await GetSellerDetailBySellerId(parseInt(id));
    setSellerData(response.data[0]);
  }

  useEffect(() => {
    GetSellerDetail();
  }, []);

  async function ClickHandler(mode) {
    if (mode === "Cancle") {
      history.push("/sellerApproval");
    } else if (mode === "Approved") {

    } else if (mode === "Reject") {
       
    }
  }

  return (
    <>
      {sellerData && (
        <Card>
          <CardHeader className="text-center">
            <h5>Seller Details</h5>
          </CardHeader>
          <CardBody>
            <Row>
              <Col lg="8" md="8" sm="12" xs="12">
                <table className="table table-bordered">
                  <tr>
                    <th colSpan="4">
                      <div className="d-flex justify-content-between">
                        <h4>
                          {sellerData.storename}{" "}
                          {sellerData.sellertype == "T"
                            ? "(Traders)"
                            : sellerData.sellertype == "W"
                            ? "(Wholesaler)"
                            : sellerData.sellertype == "M"
                            ? "(Manufacturer)"
                            : ""}
                        </h4>
                        <h4>Biz Category: {sellerData.name}</h4>
                      </div>
                    </th>
                  </tr>
                  <tr>
                    <td className="h6">Tax ID: </td>
                    <td>{sellerData.taxid}</td>
                    <td className="h6">Lic Details: </td>
                    <td>{sellerData.licensedetails}</td>
                  </tr>
                  <tr>
                    <td className="h6">Factory Area: </td>
                    <td>{sellerData.factoryarea}</td>
                    <td className="h6">Warehouse Area: </td>
                    <td>{sellerData.warehousearea}</td>
                  </tr>
                  <tr>
                    <td className="h6">Workers#: </td>
                    <td>{sellerData.numberofworkers}</td>
                    <td className="h6">Production Capacity: </td>
                    <td>{sellerData.mfgcapacity}</td>
                  </tr>
                </table>
                <div className="d-none d-lg-block d-md-block">
                  <table className="table table-bordered">
                    <tr>
                      <td>
                        <span className="h6">Export:</span>
                      </td>
                      <td>
                        {sellerData.export ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>
                      <td>
                        <span className="h6">In House QC:</span>
                      </td>
                      <td>
                        {sellerData.inhouseqc ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>
                      <td>
                        <span className="h6">Packing:</span>
                      </td>
                      <td>
                        {sellerData.packing ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <span className="h6"> White Label:</span>
                      </td>
                      <td>
                        {sellerData.whitelabel ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>
                      <td>
                        <span className="h6"> Shipping:</span>
                      </td>
                      <td>
                        {sellerData.shipping ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>
                      <td>
                        <span className="h6"> Cutomization:</span>
                      </td>
                      <td>
                        {sellerData.customizing ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>
                    </tr>
                  </table>
                </div>
                <div className="d-sm-block d-xs-block d-lg-none d-md-none">
                  <table className="table table-bordered">
                    <tr>
                      <td>
                        <span className="h6">Export:</span>
                      </td>
                      <td>
                        {sellerData.export ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>
                      <td>
                        <span className="h6">In House QC:</span>
                      </td>
                      <td>
                        {sellerData.inhouseqc ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <span className="h6">Packing:</span>
                      </td>
                      <td>
                        {sellerData.packing ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>

                      <td>
                        <span className="h6"> White Label:</span>
                      </td>
                      <td>
                        {sellerData.whitelabel ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <span className="h6"> Shipping:</span>
                      </td>
                      <td>
                        {sellerData.shipping ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>
                      <td>
                        <span className="h6"> Cutomization:</span>
                      </td>
                      <td>
                        {sellerData.customizing ? (
                          <Badge color="success">Yes</Badge>
                        ) : (
                          <Badge color="danger">No</Badge>
                        )}
                      </td>
                    </tr>
                  </table>
                </div>
              </Col>
              <Col lg="4" md="4" sm="12" xs="12">
                <Row className="m-0 p-0">
                  <Col
                    lg="12"
                    md="12"
                    sm="12"
                    xs="12"
                    className="d-flex align-item-center"
                  >
                    <VideoSlider />
                  </Col>
                </Row>
              </Col>
              <Col lg="8" md="8" sm="12" xs="12">
                <table className="table table-bordered">
                  <tr>
                    <th>
                      <h6>Remarks</h6>
                    </th>
                  </tr>
                  <tr>
                    <td className="p-1">
                      <FormGroup className="m-0">
                        <Input
                          type="textarea"
                          name="text"
                          rows="4"
                          placeholder="Enter note..."
                        />
                      </FormGroup>
                    </td>
                  </tr>
                </table>
                <Row>
                  <Col
                    lg="12"
                    md="12"
                    sm="12"
                    xs="12"
                    className="d-flex align-item-center justify-content-center"
                  >
                    <Button
                      color="success"
                      className="mx-2"
                      onClick={(e) => ClickHandler("Approved")}
                    >
                      Approve
                    </Button>
                    <Button
                      color="danger"
                      className="mx-2"
                      onClick={(e) => ClickHandler("Reject")}
                    >
                      Reject
                    </Button>
                    <Button
                      className="btnPrimary mx-2"
                      onClick={(e) => ClickHandler("Cancle")}
                    >
                      Back
                    </Button>
                  </Col>
                </Row>
              </Col>
              <Col lg="4" md="4" sm="12" xs="12" className="align-item-center">
                <table className="table table-bordered">
                  <tr>
                    <th>
                      <h6>About Company</h6>
                    </th>
                  </tr>
                  <tr>
                    <td>
                      <span style={{ fontSize: "15px" }}>
                        {sellerData.aboutcompany}
                      </span>
                    </td>
                  </tr>
                </table>
              </Col>
            </Row>
          </CardBody>
        </Card>
      )}
    </>
  );
};

export default SellerDetails;
