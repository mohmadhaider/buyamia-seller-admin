import React, { useState } from "react";
import "../SellerApproval/VideoSlider.scss";

const VideoSlider = () => {
  const [currentVideo, setCurrentVideo] = useState(0);

  function ChangeSlide(slideindex) {
    const nextIndex = currentVideo + slideindex;
    if (nextIndex > 3) {
      setCurrentVideo(0);
    } else if (nextIndex < 0) {
      setCurrentVideo(3);
    } else {
      setCurrentVideo(currentVideo + slideindex);
    }
  }

  return (
    <>
      <div className={"slideshowContainer"}>
        {currentVideo == 0 && (
          <div className={"anim"}>
            <video
              autoPlay
              controls
              style={{
                verticalAlign: "middle",
                width: "100%",
                height: "auto",
              }}
            >
              <source
                src="https://ak.picdn.net/shutterstock/videos/1058229220/preview/stock-footage-an-young-mailman-courier-with-a-protective-mask-and-gloves-is-delivering-a-parcel-directly-to-a.mp4"
                type="video/mp4"
              ></source>
            </video>
            <div className={"text"}>Factory Video</div>
          </div>
        )}

        {currentVideo == 1 && (
          <div className={"anim"}>
            <video
              autoPlay
              controls
              style={{
                verticalAlign: "middle",
                width: "100%",
                height: "auto",
              }}
            >
              <source
                src="https://ak.picdn.net/shutterstock/videos/1056761975/preview/stock-footage-graceful-female-hand-takes-a-glass-of-red-wine-from-the-table-light-background.mp4"
                type="video/mp4"
              ></source>
            </video>
            <div className={"text"}>Product Sample</div>
          </div>
        )}

        {currentVideo == 2 && (
          <div className={"anim"}>
            <video
              autoPlay
              controls
              style={{
                verticalAlign: "middle",
                width: "100%",
                height: "auto",
              }}
            >
              <source
                src="https://ak.picdn.net/shutterstock/videos/1058229220/preview/stock-footage-an-young-mailman-courier-with-a-protective-mask-and-gloves-is-delivering-a-parcel-directly-to-a.mp4"
                type="video/mp4"
              ></source>
            </video>
            <div className={"text"}>Buyer Testimonial</div>
          </div>
        )}

        {currentVideo == 3 && (
          <div className={"anim"}>
            <video
              autoPlay
              controls
              style={{
                verticalAlign: "middle",
                width: "100%",
                height: "auto",
              }}
            >
              <source
                src="https://ak.picdn.net/shutterstock/videos/1058229220/preview/stock-footage-an-young-mailman-courier-with-a-protective-mask-and-gloves-is-delivering-a-parcel-directly-to-a.mp4"
                type="video/mp4"
              ></source>
            </video>
            <div className={"text"}>Buyer Testimonial</div>
          </div>
        )}

        <a
          className={"prev"}
          href="javascript:void(0)"
          onClick={(e) => ChangeSlide(-1)}
        >
          &#10094;
        </a>
        <a
          className={"next"}
          href="javascript:void(0)"
          onClick={(e) => ChangeSlide(1)}
        >
          &#10095;
        </a>
      </div>
    </>
  );
};

export default VideoSlider;
