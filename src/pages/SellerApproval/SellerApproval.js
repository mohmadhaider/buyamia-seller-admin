import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import MUIDataTable from "mui-datatables";
import { useSellerData } from "../../services/sellerService";
import { Card, CardBody, CardHeader, Button } from "reactstrap";
import Filter from "./Filter";

const columns = [
  {
    label: "Regd. On",
    name: "regDate",
    options: {
      filter: true,
      sort: false,
    },
  },
  {
    label: "Time",
    name: "regTime",
    options: {
      filter: true,
      sort: false,
    },
  },
  {
    label: "Seller",
    name: "storename",
    options: {
      filter: true,
      sort: false,
    },
  },
  {
    label: "Type",
    name: "SellerType",
    options: {
      filter: true,
      sort: false,
    },
  },
  {
    label: "Company Type",
    name: "companytype",
    options: {
      filter: true,
      sort: false,
    },
  },
  {
    label: "Contact#",
    name: "phone1",
    options: {
      filter: true,
      sort: false,
    },
  },
  {
    label: "City",
    name: "city",
    options: {
      filter: true,
      sort: false,
    },
  },
  {
    label: "State",
    name: "STATE",
    options: {
      filter: true,
      sort: false,
    },
  },
  {
    label: "Country",
    name: "country",
    options: {
      filter: true,
      sort: false,
    },
  },
  {
    label: "Biz Type",
    name: "bizCategory",
    options: {
      filter: true,
      sort: false,
    },
  },
  {
    label: "Status",
    name: "STATUS",
    options: {
      filter: true,
      sort: false,
    },
  },
  {
    label: "Action",
    name: "Action",
    options: {
      filter: true,
      sort: false,
    },
  },
];

const SellerApproval = () => {
  const [sellersData, setSellersData] = useState(null);
  const { GetAllSellers } = useSellerData();
  const history = useHistory();
  async function GetAllSellerData() {
    let sellerFilterData = { fromdate: "", todate: "" };
    let sellersData = await GetAllSellers(sellerFilterData);
    if (sellersData.status) {
      let newsellerData = sellersData.data.map((sellerdata) => ({
        ...sellerdata,
        Action: (
          <Button
            className="btnPrimary"
            onClick={() => ApprovalSeller(sellerdata)}
          >
            Action
          </Button>
        ),
      }));
      setSellersData(newsellerData);
    }
  }
  const options = {
    filter: false,
    selectableRowsHideCheckboxes: true,
    sortFilterList: true,
  };

  useEffect(() => {
    GetAllSellerData();
  }, []);

  const ApprovalSeller = (sellerdata) => {
    console.log(sellerdata);
    history.push("/SellerApproval/" + sellerdata.sellerid);
  };
  return (
    <>
      <Card>
        <CardHeader>
          <div className="text-center">
            <h3>Seller Approval</h3>
          </div>
        </CardHeader>
        <CardBody className="p-0">
          <Filter />
          {sellersData && (
            <MUIDataTable
              data={sellersData}
              columns={columns}
              options={options}
            ></MUIDataTable>
          )}
        </CardBody>
      </Card>
    </>
  );
};

export default SellerApproval;
