import React from "react";
import { Input, Row, Col, Label, FormGroup, Button, Card, CardBody } from "reactstrap";

const Filter = () => {
  return (
    <Card>
      <CardBody className="p-2">
        <Row className="p-0">
          <Col lg="2" md="2" sm="6" xs="6">
            <FormGroup>
              <Label className="mb-0">Status</Label>
              <Input type="select">
                <option value="" hidden></option>
                <option value="">All</option>
                <option value="P" selected>
                  Submitted
                </option>
                <option value="A">Apporoved</option>
                <option value="R">Rejected</option>
              </Input>
            </FormGroup>
          </Col>
          <Col lg="2" md="2" sm="6" xs="6" className="">
            <FormGroup>
              <Label className="mb-0">&nbsp;</Label>
              <Button className="btnPrimary" block>
                Retrieve
              </Button>
            </FormGroup>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

export default Filter;
