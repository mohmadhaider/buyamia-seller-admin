import React, { useState } from "react";
import PropTypes from "prop-types";
import { withRouter, Redirect, Link, useHistory } from "react-router-dom";
import { RegisterService } from "../../services/RegisterService";
// import { connect } from "react-redux";
import {
  Container,
  Row,
  Col,
  Button,
  FormGroup,
  FormText,
  Input,
} from "reactstrap";
import Widget from "../../components/Widget/Widget.js";
import Footer from "../../components/Footer/Footer.js";

import loginImage from "../../assets/registerImage.svg";
import SofiaLogo from "../../components/Icons/SofiaLogo.js";
// import { registerUser } from "../../actions/register.js";
// import hasToken from "../../services/authService";

// import GoogleIcon from "../../components/Icons/AuthIcons/GoogleIcon.js";
// import TwitterIcon from "../../components/Icons/AuthIcons/TwitterIcon.js";
// import FacebookIcon from "../../components/Icons/AuthIcons/FacebookIcon.js";
// import GithubIcon from "../../components/Icons/AuthIcons/GithubIcon.js";
// import LinkedinIcon from "../../components/Icons/AuthIcons/LinkedinIcon.js";

const Register = (props) => {
  const { RegisterUser } = RegisterService();
  const [register, setRegister] = useState({
    firstname: "",
    lastname:"",
    emailid: "",
    password: "",
    companytype:"",
    phone1:"",
  })
  const [confirmPassword, setConfirmPassword] = useState("")
  const history = useHistory();
  let handleChange = (e) => {
    setRegister({ ...register, [e.target.name]: e.target.value });
  }

  const doRegister = async (event) => {
    event.preventDefault();
    if (register.password === confirmPassword) {
      let responseData = await RegisterUser(register)
      localStorage.setItem("RegisterData",JSON.stringify(responseData));
      history.push('/home');
    } else {
      alert(" please try again !");
    }
  }

  return (
    <div className="auth-page">
      <Container className="col-12">
        <Row className="d-flex align-items-center">
          <Col xs={12} lg={6} className="left-column">
            <Widget className="widget-auth widget-p-lg">
              <div className="d-flex align-items-center justify-content-between py-3">
                <p className="auth-header mb-0">Sign Up</p>
                <div className="logo-block">
                  <SofiaLogo />
                </div>
              </div>
              <form onSubmit={(event => doRegister(event))}>
                <FormGroup className="my-3">
                  <FormText>First Name</FormText>
                  <Input
                    id="name"
                    className="input-transparent pl-3"
                    value={register.firstname}
                    onChange={(e) => { handleChange(e) }}
                    type="text"
                    required
                    name="firstname"
                    placeholder="Enter Your First Name "
                  />
                </FormGroup>
                <FormGroup className="my-3">
                  <FormText>Last Name</FormText>
                  <Input
                    id="name"
                    className="input-transparent pl-3"
                    value={register.lastname}
                    onChange={(e) => { handleChange(e) }}
                    type="text"
                    required
                    name="lastname"
                    placeholder="Enter Your Last Name "
                  />
                </FormGroup>
                <FormGroup className="my-3">
                  <div className="d-flex justify-content-between">
                    <FormText>Phone Nmber</FormText>
                  </div>
                  <Input
                    id="phonenumber"
                    className="input-transparent pl-3"
                    value={register.phone1}
                    onChange={(e) => { handleChange(e) }}
                    type="text"
                    required
                    name="phone1"
                    placeholder="Enter your Phone number "
                  />
                </FormGroup>
                <FormGroup className="my-3">
                  <FormText>Email Id</FormText>
                  <Input
                    id="email"
                    className="input-transparent pl-3"
                    value={register.emailid}
                    onChange={(e) => { handleChange(e) }}
                    type="email"
                    required
                    name="emailid"
                    placeholder="Enter Your Email Address "
                  />
                </FormGroup>
                <FormGroup className="my-3">
                  <div className="d-flex justify-content-between">
                    <FormText>Password</FormText>
                  </div>
                  <Input
                    id="password"
                    className="input-transparent pl-3"
                    value={register.password}
                    onChange={(e) => { handleChange(e) }}
                    type="password"
                    required
                    name="password"
                    placeholder="Enter your password "
                  />
                </FormGroup>
                <FormGroup className="my-3">
                  <div className="d-flex justify-content-between">
                    <FormText>Confirm Password</FormText>
                  </div>
                  <Input
                    id="confirmPassword"
                    className="input-transparent pl-3"
                    value={confirmPassword}
                    onChange={(e) => { setConfirmPassword(e.target.value) }}
                    type="Password"
                    required
                    name="confirmPassword"
                    placeholder="Enter your Confirm password "
                  />
                </FormGroup>
                <FormGroup className="my-3">
                  <div className="d-flex justify-content-between">
                    <FormText>Compant Type</FormText>
                  </div>
                  <select class="form-control" name='companytype' value={register.companytype} onChange={(e) => { handleChange(e) }}>
                            <option value="0" selected>Select Company Type</option>
                            <option value="Firma (Fa)">Firma (Fa)</option>
                            <option value="Commanditaire Vennotschap (CV)">Commanditaire Vennotschap (CV)</option>
                            <option value="Limited Company(PT)">Limited Company(PT)</option>
                            <option value="Public Company (Perum-Perusahaan Umum)">Public Company (Perum-Perusahaan Umum)</option>
                            <option value="Liability Company (Persero)">Liability Company (Persero)</option>
                        </select>
                </FormGroup>
                <div className="bg-widget d-flex justify-content-center">
                  <Button className="rounded-pill my-3" type="submit" color="secondary-red">Next</Button>
                </div>
                {/* <p className="dividing-line my-3">&#8195;Or&#8195;</p>
                <div className="d-flex align-items-center my-3">
                  <p className="social-label mb-0">Login with</p>
                  <div className="socials">
                    <a href="https://flatlogic.com/"><GoogleIcon /></a>
                    <a href="https://flatlogic.com/"><TwitterIcon /></a>
                    <a href="https://flatlogic.com/"><FacebookIcon /></a>
                    <a href="https://flatlogic.com/"><GithubIcon /></a>
                    <a href="https://flatlogic.com/"><LinkedinIcon /></a>
                  </div>
                </div>
                <Link to="/login">Enter the account</Link> */}
              </form>
            </Widget>
          </Col>
          <Col xs={0} lg={6} className="right-column">
            <div>
              <img src={loginImage} alt="Error page" />
            </div>
          </Col>
        </Row>
      </Container>
      <Footer />
    </div>
  )
}

// Register.propTypes = {
//   dispatch: PropTypes.func.isRequired,
// }

// function mapStateToProps(state) {
//   return {
//     isFetching: state.auth.isFetching,
//     isAuthenticated: state.auth.isAuthenticated,
//     errorMessage: state.auth.errorMessage,
//   };
// }

export default Register;
