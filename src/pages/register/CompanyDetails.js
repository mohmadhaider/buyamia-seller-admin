import React, { useState, useEffect } from 'react'
import { useProfileData } from '../../services/ProfileService'
import Axios from "axios";
import {
    Container,
    Row,
    Col,
    Button,
    FormGroup,
    FormText,
    Input,
} from "reactstrap";
import Widget from "../../components/Widget/Widget.js";
import Footer from "../../components/Footer/Footer.js";
import SofiaLogo from "../../components/Icons/SofiaLogo.js";
import { useHistory } from 'react-router-dom';


const CompanyDetails = () => {
    const [country, setCountry] = useState([]);
    const [city, setCity] = useState([]);
    const [state, setState] = useState([]);
    const [selectCountry, setSelectCountry] = useState("");
    const [selectState, setSelectState] = useState("");
    const [selectCity, setSelectCity] = useState("");

    const history = useHistory();

    useEffect(() => {
        getBusinessCategory();
        Axios.get("https://countriesnow.space/api/v0.1/countries/capital").then(
            (data) => {
                setCountry(data.data.data);
            }
        );
    }, []);

    const onChangeCountry = (e) => {
        setState([]);
        setSelectCountry(e.target.value);
        Axios.post("https://countriesnow.space/api/v0.1/countries/states", {
            country: e.target.value,
        }).then((data) => {
            //   console.log(data.data.data.);
            setState(data.data.data.states);
        });
    };
    const onChangeState = (e) => {
        setCity([]);
        setSelectState(e.target.value);
        Axios.post("https://countriesnow.space/api/v0.1/countries/state/cities", {
            country: selectCountry,
            state: e.target.value,
        }).then((data) => {
            //   console.log(data.data.data);
            setCity(data.data.data);
        });
    };

    const onChangeCity = (e) => {
        setSelectCity(e.target.value);
    };

    const { GetBusinessCategory } = useProfileData();
    const [businessCategory, setBusinessCategory] = useState();
    const [companyDetails, setCompanyDetails] = useState({
        contactpersonname: "",
        contactperonnumber:"",
        businessname: "",
        companytype:"",
        businesscategory: "",
        address: "",
        zipcode: "",
        city:"",
        state:"",
        country:""
    })

    let getBusinessCategory = async () => {
        let finalarray = [];
        let BusinessCategoryData = await GetBusinessCategory()
        BusinessCategoryData.data.filter((category) => {
            if (category.hassubcategory) {
                finalarray.push(category);
                console.log(finalarray)
            }
        })
        setBusinessCategory(finalarray);

    }

    let submitCompanyDetails = () => {
        history.push('/profile');
    }

    let handleChange = (e) => {
        setCompanyDetails({ ...companyDetails, [e.target.Name]: e.target.value });
    }

    return (
        <div className="p-8" style={{
            backgroundColor: "#fff"
        }}>
            <center className="">
                <p className="auth-header" style={{
                    fontSize: "24px",
                    marginTop: 10
                }}>Company Details</p>
                {/* <div className="logo-block">
                                    <SofiaLogo />
                                </div> */}
            </center>
            <Container className="">
                <Row className="d-flex justify-content-center align-items-center"
                    style={{
                        padding: "30px"
                    }}>

                    <Col xs={5} lg={5} md={5} style={{
                    }}>
                        <div >

                            <form onSubmit={(event => submitCompanyDetails(event))}>
                                <FormGroup className="my-3">
                                    <div className="d-flex justify-content-between">
                                        <FormText>Contact Person Name</FormText>
                                    </div>
                                    <Input
                                        id="password"
                                        className="input-transparent pl-3"
                                        value={companyDetails.contactpersonname}
                                        onChange={(e) => { handleChange(e) }}
                                        type="text"
                                        required
                                        name="contactpersonname"
                                        placeholder="Place your Contact Person Name here"
                                    />
                                </FormGroup>
                                <FormGroup className="my-3">
                                    <div className="d-flex justify-content-between">
                                        <FormText>Contact Person Number</FormText>
                                    </div>
                                    <Input
                                        id="password"
                                        className="input-transparent pl-3"
                                        value={companyDetails.contactperonnumber}
                                        onChange="{(e) => { handleChange(e) }}"
                                        type="text"
                                        required
                                        name="contactperonnumber"
                                        placeholder="Place your Contact Person Number here"
                                    />
                                </FormGroup>
                                <FormGroup className="my-3">
                                    <FormText>Company Name</FormText>
                                    <Input
                                        id="name"
                                        className="input-transparent pl-3"
                                        value=""
                                        onChange="{(e) => { handleChange(e) }}"
                                        type="text"
                                        required
                                        name="businessname"
                                        placeholder="Place Your Company Name Here"
                                    />
                                </FormGroup>
                                <FormGroup className="my-3">
                                    <div className="d-flex justify-content-between">
                                        <FormText>Select Company Type</FormText>
                                    </div>
                                    <select
                                        class="form-control"
                                        name="companytype"
                                        value={companyDetails.companytype}
                                        onChange={(e) => { handleChange(e) }}
                                    >
                                        <option value="0" selected>
                                            Select Company Type
                                        </option>
                                        {/* {state?.map((tempState) => (
                                            <option value={tempState.name}>{tempState.name}</option>
                                        ))} */}
                                    </select>
                                </FormGroup>
                                <FormGroup className="my-3">
                                    <div className="d-flex justify-content-between">
                                        <FormText>Select Business Type</FormText>
                                    </div>

                                    <select
                                        class="form-control"
                                        name="businessCategory"
                                    value={companyDetails.businesscategory}
                                    onChange="{(e)=>{setBusinessCategory(e.target.value)}}"
                                    >
                                        <option value="0" selected>
                                            Select Business Type
                                        </option>
                                        {

                                            businessCategory && businessCategory.map((category) => (
                                                <option value={category.categoryid}>{category.name}</option>
                                            ))
                                        }

                                    </select>
                                </FormGroup>
                            </form>
                        </div>
                    </Col>
                    <Col xs={5} lg={5} md={5} style={{
                    }}>
                        <div >

                            <form onSubmit={(event => submitCompanyDetails(event))}>
                                <FormGroup className="my-3">
                                    <div className="d-flex justify-content-between">
                                        <FormText>Address</FormText>
                                    </div>
                                    <Input
                                        id="address"
                                        className="input-transparent pl-3"
                                        value={companyDetails.address}
                                        onChange={(e) => { handleChange(e) }}
                                        type="text"
                                        required
                                        name="address"
                                        placeholder="Place your Address here (Street, Landmark)"
                                    />
                                </FormGroup>
                                <FormGroup className="my-3">
                                    <div className="d-flex justify-content-between">
                                        <FormText>Country</FormText>
                                    </div>
                                    <select
                                        class="form-control"
                                        name="selectCountry"
                                        value={selectCountry}
                                        onChange={onChangeCountry}
                                    >
                                        <option value="0" selected>
                                            Select Country
                                        </option>
                                        {country?.map((tempCountry) => (
                                            <option value={tempCountry.name}>{tempCountry.name}</option>
                                        ))}
                                    </select>
                                </FormGroup>
                                <FormGroup className="my-3">
                                    <div className="d-flex justify-content-between">
                                        <FormText>State</FormText>
                                    </div>
                                    <select
                                        class="form-control"
                                        name="selectCountry"
                                        value={selectState}
                                        onChange={onChangeState}
                                    >
                                        <option value="0" selected>
                                            Select State
                                        </option>
                                        {state?.map((tempState) => (
                                            <option value={tempState.name}>{tempState.name}</option>
                                        ))}
                                    </select>
                                </FormGroup>
                                <FormGroup className="my-3">
                                    <div className="d-flex justify-content-between">
                                        <FormText>City</FormText>
                                    </div>
                                    <select
                                        class="form-control"
                                        name="selectCountry"
                                        value={selectCity}
                                        onChange={onChangeCity}
                                    >
                                        <option value="0" selected>
                                            Select City
                                        </option>
                                        {city?.map((tempCity) => (
                                            <option value={tempCity}>{tempCity}</option>
                                        ))}
                                    </select>
                                </FormGroup>
                                <FormGroup className="my-3">
                                    <div className="d-flex justify-content-between">
                                        <FormText>Postal Code</FormText>
                                    </div>
                                    <Input
                                        id="confirmPassword"
                                        className="input-transparent pl-3"
                                        value={companyDetails.zipcode}
                                        onChange={(e) => { handleChange(e) }}
                                        type="text"
                                        required
                                        name="zipcode"
                                        placeholder="Place your postal here"
                                    />
                                </FormGroup>
                            </form>
                        </div>
                    </Col>
                </Row>
            </Container>
            <div className="bg-widget d-flex justify-content-center">
                <Button className="btnPrimary my-3" type="submit">Submit</Button>
            </div>
        </div>
    )
}

export default CompanyDetails
