import axios from "../utils/axios";

const GetAllSellers = async (sellerFilterData) => {
  try {
    const response = await axios.request({
      url: "GetAllSellerDetails",
      method: "post",
      data: sellerFilterData,
    });
    return response.data;
  } catch (error) {
    console.error(error.message);
    return false;
  }
};

const GetSellerDetailBySellerId = async (sellerid) => {
  try {
    debugger
    const response = await axios.request({
      url: "getsingleseller",
      method: "post",
      data: {
        sellerid: sellerid,
      },
    });
    return response.data;
  } catch (error) {
    console.error(error.message);
    return false;
  }
};

export const useSellerData = () => ({
  GetAllSellers,
  GetSellerDetailBySellerId,
});
