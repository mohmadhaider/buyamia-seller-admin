import axios from '../utils/axios';

const hasAuthToken = async (token) => {
  try {
    const response = await axios.request({
      url: "/getuserdatabyauthtoken",
      method: "post",
      headers:token,
      data: JSON.stringify({}),
    });

    const portalUser = response.data[0];

    return portalUser;
  } catch (error) {
    return false;
  }
}

export const AuthTokenService = () => ({hasAuthToken});
