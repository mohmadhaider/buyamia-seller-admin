import axios from '../utils/axios';

const RegisterUser = async (registerData) => {
    try {
      const response = await axios.request({
        url: "/insertportaluser",
        method: "post",
        data: registerData,
      });
  
      const userData = response.data[0];
  
      return userData;
    } catch (error) {
      return false;
    }
  };

  export const RegisterService = () => ({RegisterUser});