import axios from '../utils/axios';

const InsertSeller = async (sellerData) => {
    try {
      const response = await axios.request({
        url: "/insertSeller",
        method: "post",
        data: sellerData,
      });
  
      const userData = response.data[0];
  
      return userData;
    } catch (error) {
      return false;
    }
  };


  const GetSellerProfile = async (sellerid) => {
      
    try {
      const response = await axios.request({
        url: "/getsingleseller",
        method: "post",
        data: {
            sellerid: sellerid
        },
      });
  
      const userData = response.data;
      return userData;
    } catch (error) {
      return false;
    }
  };

  const GetBusinessCategory = async (sellerid) => {
      
    try {
      const response = await axios.request({
        url: "/getbusinesscategory",
        method: "get"
      });
  
      const userData = response.data;
      return userData;
    } catch (error) {
      return false;
    }
  };



export const useProfileData = () => ({InsertSeller, GetSellerProfile,GetBusinessCategory});
