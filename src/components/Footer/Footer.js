import React from "react";
import s from "./Footer.module.scss";

const Footer = () => {
  return (
    <div className="d-flex justify-content-center mb-3">
    <div className={s.footer}>
      <h6 className={s.footerLabel}>
        2021 &copy; Buyamia.com
      </h6>
    </div>
    </div>
  );
}

export default Footer;
